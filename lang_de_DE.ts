<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>APIXmlReceiver</name>
    <message>
        <location filename="../APIXmlReceiver.cpp" line="105"/>
        <source>Couldn&apos;t convert %1 to %2!</source>
        <translation>Konnte %1 nicht in %2 umwandeln!</translation>
    </message>
</context>
<context>
    <name>DatabaseUtils</name>
    <message>
        <source>Error crating DB object!</source>
        <translatorcomment>#TYPO# There&apos;s a spelling error in the original text.</translatorcomment>
        <translation type="vanished">Fehler beim Erstellen des DB Objekts!</translation>
    </message>
    <message>
        <location filename="../DatabaseUtils.cpp" line="40"/>
        <source>Error creating DB object!</source>
        <translation>Fehler beim Erstellen des DB Objekts!</translation>
    </message>
    <message>
        <location filename="../DatabaseUtils.cpp" line="47"/>
        <source>Error creating DB path!</source>
        <translation>Fehler beim Erstellen des DB Pfads!</translation>
    </message>
    <message>
        <location filename="../DatabaseUtils.cpp" line="51"/>
        <source>Error opening DB!</source>
        <translation>Fehler beim Öffnen der DB!</translation>
    </message>
</context>
<context>
    <name>Evernus</name>
    <message>
        <source>Proxy</source>
        <translation type="obsolete">Proxy</translation>
    </message>
    <message>
        <source>No proxy</source>
        <translation type="obsolete">Kein Proxy</translation>
    </message>
    <message>
        <source>Custom proxy</source>
        <translation type="obsolete">Benutzerdefinierter Proxy</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="obsolete">Typ:</translation>
    </message>
    <message>
        <source>SOCKS5</source>
        <translation type="obsolete">SOCKS5</translation>
    </message>
    <message>
        <source>HTTP</source>
        <translation type="obsolete">HTTP</translation>
    </message>
    <message>
        <source>Host:</source>
        <translation type="obsolete">Host:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation type="obsolete">Port:</translation>
    </message>
    <message>
        <source>User:</source>
        <translation type="obsolete">Benutzer:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation type="obsolete">Paßwort:</translation>
    </message>
    <message>
        <source>Warning: password store uses weak encryption - do not use sensitive passwords.</source>
        <translation type="obsolete">Warnung: Paßwörter werden mit einer schwacher Verschlüsselung gespeichert - benutze keine sensiblen Paßwörter.</translation>
    </message>
    <message>
        <source>API provider</source>
        <translation type="obsolete">API Anbieter</translation>
    </message>
    <message>
        <source>Use default provider</source>
        <translation type="obsolete">Standardanbieter benutzen</translation>
    </message>
    <message>
        <source>Use custom provider</source>
        <translation type="obsolete">Benutzerdefinierten Anbieter benutzen</translation>
    </message>
    <message>
        <source>CREST</source>
        <translation type="obsolete">CREST</translation>
    </message>
    <message>
        <source>Max. threads:</source>
        <translation type="obsolete">Max. Threads:</translation>
    </message>
    <message>
        <source>This value affects the speed of importing data via CREST. Higher number gives more speed, but too high value can cause the speed to drop and/or create import errors.</source>
        <translation type="obsolete">Dieser Wert beeinflußt die Geschwindigkeit des Datenimports via CREST. Ein höherer Wert erhöht die Geschwindigkeit, ist er jedoch zu hoch, kann die Geschwindigkeit sogar sinken oder Fehler verursachen.</translation>
    </message>
    <message>
        <source>Custom station</source>
        <translation type="obsolete">Benutzerdefinierte Station</translation>
    </message>
</context>
<context>
    <name>Evernus::APIInterface</name>
    <message>
        <source>Encountered SSL errors:

%1</source>
        <translation type="vanished">Aufgetretene SSL Fehler:

%1</translation>
    </message>
</context>
<context>
    <name>Evernus::APIManager</name>
    <message>
        <location filename="../APIManager.cpp" line="603"/>
        <source>Invalid XML document received!</source>
        <translation>Ungültiges XML Dokument erhalten!</translation>
    </message>
    <message>
        <location filename="../APIManager.cpp" line="626"/>
        <source>No XML document received!</source>
        <translation>Kein XML Dokument erhalten!</translation>
    </message>
</context>
<context>
    <name>Evernus::APIResponseCache</name>
    <message>
        <location filename="../APIResponseCache.cpp" line="54"/>
        <source>Cannot create cache path.</source>
        <translation>Konnte das Cacheverzeichnis nicht erstellen.</translation>
    </message>
</context>
<context>
    <name>Evernus::AboutDialog</name>
    <message>
        <source>&lt;strong&gt;%1&lt;/strong&gt;&lt;br /&gt;%2&lt;br /&gt;&lt;br /&gt;Created by &lt;strong&gt;&lt;a href=&apos;http://evewho.com/pilot/Pete+Butcher&apos;&gt;Pete Butcher&lt;/a&gt;&lt;/strong&gt;&lt;br /&gt;All donations are welcome :)&lt;br /&gt;&lt;br /&gt;&lt;a href=&apos;%3&apos;&gt;%3&lt;/a&gt;&lt;br /&gt;Twitter: &lt;a href=&apos;http://twitter.com/evernusproject&apos;&gt;@evernusproject&lt;/a&gt;&lt;br /&gt;Forum topic: &lt;a href=&apos;%4&apos;&gt;%4&lt;/a&gt;</source>
        <translation type="vanished">&lt;strong&gt;%1&lt;/strong&gt;&lt;br /&gt;%2&lt;br /&gt;&lt;br /&gt;Erstellt von &lt;strong&gt;&lt;a href=&apos;http://evewho.com/pilot/Pete+Butcher&apos;&gt;Pete Butcher&lt;/a&gt;&lt;/strong&gt;&lt;br /&gt;Jede Spende ist willkommen :)&lt;br /&gt;&lt;br /&gt;&lt;a href=&apos;%3&apos;&gt;%3&lt;/a&gt;&lt;br /&gt;Twitter: &lt;a href=&apos;http://twitter.com/evernusproject&apos;&gt;@evernusproject&lt;/a&gt;&lt;br /&gt;Forumsbeitrag: &lt;a href=&apos;%4&apos;&gt;%4&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../AboutDialog.cpp" line="37"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt;&lt;br /&gt;%2&lt;br /&gt;&lt;br /&gt;Created by &lt;strong&gt;&lt;a href=&apos;http://evewho.com/pilot/Pete+Butcher&apos;&gt;Pete Butcher&lt;/a&gt;&lt;/strong&gt;&lt;br /&gt;German translation by &lt;strong&gt;&lt;a href=&apos;http://evewho.com/pilot/Hel+O%27Ween&apos;&gt;Hel O&apos;Ween&lt;/a&gt;&lt;/strong&gt;&lt;br /&gt;All donations are welcome :)&lt;br /&gt;&lt;br /&gt;&lt;a href=&apos;%3&apos;&gt;%3&lt;/a&gt;&lt;br /&gt;Twitter: &lt;a href=&apos;http://twitter.com/evernusproject&apos;&gt;@evernusproject&lt;/a&gt;&lt;br /&gt;Forum topic: &lt;a href=&apos;%4&apos;&gt;%4&lt;/a&gt;</source>
        <translation>&lt;strong&gt;%1&lt;/strong&gt;&lt;br /&gt;%2&lt;br /&gt;&lt;br /&gt;Erstellt von &lt;strong&gt;&lt;a href=&apos;http://evewho.com/pilot/Pete+Butcher&apos;&gt;Pete Butcher&lt;/a&gt;&lt;/strong&gt;&lt;br /&gt;Deutsch von: &lt;strong&gt;&lt;a href=&apos;http://evewho.com/pilot/Hel+O%27Ween&apos;&gt;Hel O&apos;Ween&lt;/a&gt;&lt;/strong&gt;&lt;br /&gt;Jede Spende ist willkommen :)&lt;br /&gt;&lt;br /&gt;&lt;a href=&apos;%3&apos;&gt;%3&lt;/a&gt;&lt;br /&gt;Twitter: &lt;a href=&apos;http://twitter.com/evernusproject&apos;&gt;@evernusproject&lt;/a&gt;&lt;br /&gt;Forumsbeitrag: &lt;a href=&apos;%4&apos;&gt;%4&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../AboutDialog.cpp" line="54"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
</context>
<context>
    <name>Evernus::ActiveTasksDialog</name>
    <message>
        <location filename="../ActiveTasksDialog.cpp" line="63"/>
        <source>Please wait...</source>
        <translation>Bitte warten ...</translation>
    </message>
    <message>
        <location filename="../ActiveTasksDialog.cpp" line="75"/>
        <source>Close automatically</source>
        <translation>Automatisch schließen</translation>
    </message>
    <message>
        <location filename="../ActiveTasksDialog.cpp" line="96"/>
        <source>Active Tasks</source>
        <translation>Aktive Aufgaben</translation>
    </message>
</context>
<context>
    <name>Evernus::AggregatedStatisticsModel</name>
    <message>
        <location filename="../AggregatedStatisticsModel.cpp" line="87"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../AggregatedStatisticsModel.cpp" line="89"/>
        <source>Count</source>
        <translation>Anzahl</translation>
    </message>
    <message>
        <location filename="../AggregatedStatisticsModel.cpp" line="91"/>
        <source>Price sum</source>
        <translation>Preissumme</translation>
    </message>
    <message>
        <location filename="../AggregatedStatisticsModel.cpp" line="93"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
</context>
<context>
    <name>Evernus::AssetModel</name>
    <message>
        <location filename="../AssetModel.cpp" line="132"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../AssetModel.cpp" line="133"/>
        <source>Quantity</source>
        <translation>Menge</translation>
    </message>
    <message>
        <location filename="../AssetModel.cpp" line="134"/>
        <source>Unit volume</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../AssetModel.cpp" line="135"/>
        <source>Total volume</source>
        <translation>Größe gesamt</translation>
    </message>
    <message>
        <location filename="../AssetModel.cpp" line="136"/>
        <source>Local unit sell price</source>
        <translation>Lokaler Einzelverkaufspreis</translation>
    </message>
    <message>
        <location filename="../AssetModel.cpp" line="137"/>
        <source>Local total sell price</source>
        <translation>Lokaler Gesamtverkaufspreis</translation>
    </message>
    <message>
        <location filename="../AssetModel.cpp" line="138"/>
        <source>Owner</source>
        <translation>Besitzer</translation>
    </message>
    <message>
        <location filename="../AssetModel.cpp" line="219"/>
        <source>Price update time: %1</source>
        <translation>Letztes Preisupdate: %1</translation>
    </message>
</context>
<context>
    <name>Evernus::AssetsImportPreferencesWidget</name>
    <message>
        <location filename="../AssetsImportPreferencesWidget.cpp" line="34"/>
        <source>Assets import</source>
        <translation>Import von Gegenständen</translation>
    </message>
    <message>
        <location filename="../AssetsImportPreferencesWidget.cpp" line="39"/>
        <source>Import assets</source>
        <translation>Gegenstände importieren</translation>
    </message>
    <message>
        <location filename="../AssetsImportPreferencesWidget.cpp" line="43"/>
        <source>Store total asset value on import/price import</source>
        <translation>Gesamtwert nach Import/Preisimport speichern</translation>
    </message>
    <message>
        <location filename="../AssetsImportPreferencesWidget.cpp" line="48"/>
        <source>Store total asset value only when all prices are available</source>
        <translation>Gesamtwert nur speichern wenn alle Preise verfügbar sind</translation>
    </message>
</context>
<context>
    <name>Evernus::AssetsWidget</name>
    <message>
        <location filename="../AssetsWidget.cpp" line="65"/>
        <source>Import prices from Web</source>
        <translation>Preisimport aus dem Web</translation>
    </message>
    <message>
        <location filename="../AssetsWidget.cpp" line="70"/>
        <source>Import prices from logs</source>
        <translation>Preisimport aus Logs</translation>
    </message>
    <message>
        <location filename="../AssetsWidget.cpp" line="81"/>
        <source>Combine for all characters</source>
        <translation>Für alle Charaktere zusammenfassen</translation>
    </message>
    <message>
        <location filename="../AssetsWidget.cpp" line="111"/>
        <source>Set destination in EVE</source>
        <translation>Ziel in EVE setzen</translation>
    </message>
    <message>
        <location filename="../AssetsWidget.cpp" line="116"/>
        <source>Price station</source>
        <translation>Stationspreis</translation>
    </message>
    <message>
        <location filename="../AssetsWidget.cpp" line="124"/>
        <source>Use asset location</source>
        <translation>Verwende Standort der Ware</translation>
    </message>
    <message>
        <location filename="../AssetsWidget.cpp" line="134"/>
        <source>Use custom station</source>
        <translatorcomment>#NOTE# Correct transaltion would be &quot;Verwende benutzerdefinierte Station&quot;, but this seems a tad long.</translatorcomment>
        <translation>Verwende andere Station</translation>
    </message>
</context>
<context>
    <name>Evernus::CRESTAuthWidget</name>
    <message>
        <location filename="../CRESTAuthWidget.cpp" line="43"/>
        <source>Toggle external browser</source>
        <translation>Externen Browser verwenden</translation>
    </message>
    <message>
        <location filename="../CRESTAuthWidget.cpp" line="67"/>
        <source>To authorize inside the browser, use the following link and paste the resulting code below: &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Um sich im Browser anzumelden, benetze den folgenden Link und füge den erzeugten Code nachstehend ein: &lt;a href=&apos;%1%&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../CRESTAuthWidget.cpp" line="79"/>
        <source>paste the resulting code here</source>
        <translation>Hier den erzeugten Code einfügen</translation>
    </message>
    <message>
        <location filename="../CRESTAuthWidget.cpp" line="81"/>
        <source>Authorize</source>
        <translation>Authorisieren</translation>
    </message>
    <message>
        <location filename="../CRESTAuthWidget.cpp" line="97"/>
        <source>CREST Authentication</source>
        <translation>CREST Authentifizierung</translation>
    </message>
    <message>
        <location filename="../CRESTAuthWidget.cpp" line="98"/>
        <source>The supplied code is invalid. Please make sure all characters were copied or use internal browser authorization.</source>
        <translation>Der angegeben Code ist ungültig. Bitte stelle sicher das alle Zeichen kopiert wurden oder verwende die interne Browseranmeldung.</translation>
    </message>
    <message>
        <source>The supplied code is invalid. Please make sure all character were copied or use internal browser authorization.</source>
        <translatorcomment>#TYPO#  There&apos;s an &quot;s&quot; missing at the end of &quot;character(s)&quot;</translatorcomment>
        <translation type="vanished">Der angegeben Code ist ungültig. Bitte stelle sicher das alle Zeichen kopiert wurden oder verwende die interne Browseranmeldung.</translation>
    </message>
</context>
<context>
    <name>Evernus::CRESTExternalOrderImporter</name>
    <message>
        <location filename="../CRESTExternalOrderImporter.cpp" line="36"/>
        <source>Evernus has been compiled without CREST support. You can manually specify CREST client id and secret via command line options: --crest-id and --crest-secret</source>
        <translation>Evernus wurde ohne CREST-Unterstützung erstellt. Du kannst manuell CREST ID und CREST Secret via Kommandozeile angeben: --crest-id and --crest-secret</translation>
    </message>
    <message>
        <location filename="../CRESTExternalOrderImporter.cpp" line="88"/>
        <source>CREST import: waiting for %1 server replies</source>
        <translation>CREST Iimport: warte auf %1 Serverantworten</translation>
    </message>
</context>
<context>
    <name>Evernus::CRESTInterface</name>
    <message>
        <location filename="../CRESTInterface.cpp" line="138"/>
        <source>Empty CREST endpoint map. Please wait until endpoints have been fetched.</source>
        <translation>Leere CREST Endpunkt Karte. Bitte warten Sie, bis Endpunkte wurden geholt.</translation>
    </message>
    <message>
        <location filename="../CRESTInterface.cpp" line="188"/>
        <source>Missing CREST regions url!</source>
        <translatorcomment>#NOTE# Not sure if it makes sense to translate &quot;regions&quot; here, as it&apos;s meant as part of an API method, not a descriptive term. Better use: &quot;Fehlende CREST Regions URL&quot;</translatorcomment>
        <translation>Fehlende CREST Regionen URL!</translation>
    </message>
    <message>
        <location filename="../CRESTInterface.cpp" line="232"/>
        <source>Missing CREST item types url!</source>
        <translatorcomment>#NOTE# Same as with &quot;regions&quot;. I&apos;d rather go with &quot;Fehlende CREST Item Types URL&quot;</translatorcomment>
        <translation>Fehlende CREST Gegenstandsarten URL!</translation>
    </message>
</context>
<context>
    <name>Evernus::CRESTManager</name>
    <message>
        <location filename="../CRESTManager.cpp" line="93"/>
        <source>CREST authorization failed.</source>
        <translation>CREST-Authentifizierung fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../CRESTManager.cpp" line="261"/>
        <source>CREST Authentication</source>
        <translation>CREST Authentifizierung</translation>
    </message>
    <message>
        <location filename="../CRESTManager.cpp" line="328"/>
        <source>Empty access token!</source>
        <translation>Leeres Access Token!</translation>
    </message>
    <message>
        <location filename="../CRESTManager.cpp" line="383"/>
        <location filename="../CRESTManager.cpp" line="411"/>
        <source>CREST error</source>
        <translation>CREST Fehler</translation>
    </message>
    <message>
        <location filename="../CRESTManager.cpp" line="383"/>
        <source>EVE login page certificate contains errors:
%1
Are you sure you wish to proceed (doing so can compromise your account security)?</source>
        <translation>Das Zertifikat der EVE Loginseite enthielt Fehler: %1. Bist du sicher das du weitermachen willst (dies kann die Sicherheit deines EVE Accounts gefährden)?</translation>
    </message>
    <message>
        <location filename="../CRESTManager.cpp" line="411"/>
        <source>Error fetching CREST endpoints!</source>
        <translation>Fehler beim Abrufen der CREST Endpunkte!</translation>
    </message>
    <message>
        <location filename="../CRESTManager.cpp" line="471"/>
        <source>Empty refresh token!</source>
        <translation>Leeres Refresh Token!</translation>
    </message>
    <message>
        <location filename="../CRESTManager.cpp" line="522"/>
        <source>CREST endpoint map is empty. Please wait a while.</source>
        <translation>CREST Endpunkt Karte ist leer. Bitte warten Sie eine Weile.</translation>
    </message>
</context>
<context>
    <name>Evernus::CachingEveDataProvider</name>
    <message>
        <location filename="../CachingEveDataProvider.cpp" line="156"/>
        <location filename="../CachingEveDataProvider.cpp" line="173"/>
        <location filename="../CachingEveDataProvider.cpp" line="181"/>
        <source>(unknown)</source>
        <translation>(unbekannt)</translation>
    </message>
</context>
<context>
    <name>Evernus::CharacterBoundWidget</name>
    <message>
        <location filename="../CharacterBoundWidget.cpp" line="34"/>
        <source>API import</source>
        <translation>API Import</translation>
    </message>
    <message>
        <location filename="../CharacterBoundWidget.cpp" line="54"/>
        <location filename="../CharacterBoundWidget.cpp" line="93"/>
        <source>&lt;strong&gt;Warning!&lt;/strong&gt; No data has been imported.</source>
        <translation>&lt;strong&gt;Achtung!&lt;/strong&gt; Es wurden keine Daten importiert.</translation>
    </message>
    <message>
        <location filename="../CharacterBoundWidget.cpp" line="97"/>
        <source>&lt;strong&gt;Warning!&lt;/strong&gt; This data is %1 old and may need an update.</source>
        <translation>&lt;strong&gt;Warnung!&lt;/strong&gt; Diese Daten sind %1 alt und benötigen möglicherweise ein Update.</translation>
    </message>
</context>
<context>
    <name>Evernus::CharacterImportPreferencesWidget</name>
    <message>
        <location filename="../CharacterImportPreferencesWidget.cpp" line="33"/>
        <source>Character import</source>
        <translation>Charakterimport</translation>
    </message>
    <message>
        <location filename="../CharacterImportPreferencesWidget.cpp" line="38"/>
        <source>Import skills</source>
        <translatorcomment>#NOTE# Although &quot;skill&quot; is no German word, it&apos;s common to refer to &quot;skills&quot; in EVE even in German. Have never heard someone talking/writing about &quot;Fähigkeiten&quot; or &quot;Fertigkeiten&quot;</translatorcomment>
        <translation>Importiere Skills</translation>
    </message>
    <message>
        <location filename="../CharacterImportPreferencesWidget.cpp" line="42"/>
        <source>Import portrait</source>
        <translation>Importiere Portrait</translation>
    </message>
</context>
<context>
    <name>Evernus::CharacterManagerDialog</name>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="56"/>
        <source>Character keys</source>
        <translation>Charakter Key</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="57"/>
        <source>Corporation keys</source>
        <translation>Corporation Key</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="58"/>
        <source>Characters</source>
        <translation>Charaktere</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="70"/>
        <location filename="../CharacterManagerDialog.cpp" line="170"/>
        <source>Key ID</source>
        <translation>Key ID</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="71"/>
        <source>Character</source>
        <translation>Charakter</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="72"/>
        <location filename="../CharacterManagerDialog.cpp" line="171"/>
        <source>Verification code</source>
        <translation>Verification Code</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="74"/>
        <source>Character Manager</source>
        <translation>Charakterverwaltung
</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="213"/>
        <location filename="../CharacterManagerDialog.cpp" line="250"/>
        <source>Added keys</source>
        <translation>Hinzugefügte Keys</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="227"/>
        <location filename="../CharacterManagerDialog.cpp" line="265"/>
        <source>Add...</source>
        <translation>Hinzufügen...</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="231"/>
        <location filename="../CharacterManagerDialog.cpp" line="269"/>
        <source>Edit...</source>
        <translation>Bearbeiten...</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="236"/>
        <location filename="../CharacterManagerDialog.cpp" line="274"/>
        <location filename="../CharacterManagerDialog.cpp" line="312"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="287"/>
        <source>Available characters</source>
        <translation>Verfügbare Charaktere</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="303"/>
        <source>In order to manage characters, add keys first in the Keys tab.</source>
        <translation>Um Charaktere zu verwalten, füge zuerst ihren Key im Register Keys hinzu.</translation>
    </message>
    <message>
        <location filename="../CharacterManagerDialog.cpp" line="308"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
</context>
<context>
    <name>Evernus::CharacterModel</name>
    <message>
        <location filename="../CharacterModel.cpp" line="43"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../CharacterModel.cpp" line="45"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../CharacterModel.cpp" line="47"/>
        <source>Key id</source>
        <translation>Key ID</translation>
    </message>
</context>
<context>
    <name>Evernus::CharacterWidget</name>
    <message>
        <location filename="../CharacterWidget.cpp" line="82"/>
        <source>Character info</source>
        <translation>Charakterinfo</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="87"/>
        <source>Download portrait</source>
        <translation>Protrait herunterladen</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="117"/>
        <source>Data age</source>
        <translation>Alter der Daten</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="127"/>
        <source>Import all</source>
        <translation>Alle importieren</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="134"/>
        <source>Orders</source>
        <translation>Aufträge</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="139"/>
        <source>Buy orders:</source>
        <translation>Kaufaufträge:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="144"/>
        <location filename="../CharacterWidget.cpp" line="157"/>
        <location filename="../CharacterWidget.cpp" line="170"/>
        <source>Volume:</source>
        <translation>Volumen:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="152"/>
        <source>Sell orders:</source>
        <translation>Verkaufsaufträge:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="165"/>
        <source>Total:</source>
        <translation>Gesamt:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="178"/>
        <source>Station owner standings</source>
        <translation>Ansehen zum Stationsbesitzer</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="184"/>
        <source>Corporation standing:</source>
        <translation>Ansehen Corporation:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="189"/>
        <source>Faction standing:</source>
        <translation>Ansehen Fraktion:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="193"/>
        <source>Trade skills</source>
        <translation>Handelsskills</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="198"/>
        <source>Order amount skills</source>
        <translation>Skills Auftragsanzahl</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="203"/>
        <source>Trade:</source>
        <translation>Trade:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="204"/>
        <source>Retail:</source>
        <translation>Retail:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="205"/>
        <source>Wholesale:</source>
        <translation>Wholesale:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="206"/>
        <source>Tycoon:</source>
        <translation>Tycoon:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="208"/>
        <source>Trade range skills</source>
        <translation>Skills Auftragsreichweite</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="213"/>
        <source>Marketing:</source>
        <translation>Marketing:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="214"/>
        <source>Procurement:</source>
        <translation>Procurement:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="215"/>
        <source>Daytrading:</source>
        <translation>Daytrading:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="216"/>
        <source>Visibility:</source>
        <translation>Visibility:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="218"/>
        <source>Fee skills</source>
        <translation>Skills Gebühren</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="223"/>
        <source>Accounting:</source>
        <translation>Accounting:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="224"/>
        <source>Broker relations:</source>
        <translation>Broker relations:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="225"/>
        <source>Margin trading:</source>
        <translation>Margin Trading:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="227"/>
        <source>Contracting skills</source>
        <translation>Skills Verträge</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="232"/>
        <source>Contracting:</source>
        <translation>Contracting:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="233"/>
        <source>Corporation contracting:</source>
        <translation>Corporation Contracting:</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="275"/>
        <source>never imported</source>
        <translation>niemals importiert</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="280"/>
        <source>Character sheet: %1</source>
        <translation>Charakterblatt: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="281"/>
        <source>Asset list: %1</source>
        <translation>Gegenstandsliste: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="282"/>
        <source>Market orders: %1</source>
        <translation>Marktaufträge: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="283"/>
        <source>Wallet journal: %1</source>
        <translation>Konto-Logbuch: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="284"/>
        <source>Wallet transactions: %1</source>
        <translation>Konto-Transaktionen: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="287"/>
        <source>Contracts: %1</source>
        <translation>Verträge: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="293"/>
        <source>Corp. market orders: %1</source>
        <translation>Corp. Marktaufträge: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="294"/>
        <source>Corp. wallet journal: %1</source>
        <translation>Corp. Konto-Logbuch: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="295"/>
        <source>Corp. wallet transactions: %1</source>
        <translation>Corp. Konto-Transaktionen: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="298"/>
        <source>Corp. contracts: %1</source>
        <translation>Corp. Verträge: %1</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="456"/>
        <source>Character error</source>
        <translation>Fehler bei Charakter</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="456"/>
        <source>Character not found in DB. Refresh characters.</source>
        <translation>Charakter nicht in der DB gefunden. Aktualisiere die Charaktere.</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="534"/>
        <source>&lt;strong&gt;%1 of %2&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;%1 von %2&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../CharacterWidget.cpp" line="544"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt;</source>
        <translation>&lt;strong&gt;%1&lt;/strong&gt;</translation>
    </message>
</context>
<context>
    <name>Evernus::ContractImportPreferencesWidget</name>
    <message>
        <location filename="../ContractImportPreferencesWidget.cpp" line="34"/>
        <source>Contract import</source>
        <translation>Import von Verträgen</translation>
    </message>
    <message>
        <location filename="../ContractImportPreferencesWidget.cpp" line="39"/>
        <source>Import contracts</source>
        <translation>Verträge importiere</translation>
    </message>
</context>
<context>
    <name>Evernus::ContractModel</name>
    <message>
        <location filename="../ContractModel.cpp" line="51"/>
        <source>Issuer</source>
        <translation>Aussteller</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="53"/>
        <source>Issuer corporation</source>
        <translation>Aussteller Corporation</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="55"/>
        <source>Assignee</source>
        <translation>Beauftragter</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="57"/>
        <source>Acceptor</source>
        <translation>Empfänger</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="59"/>
        <source>Start station</source>
        <translation>Startstation</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="61"/>
        <source>End station</source>
        <translation>Zielstation</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="63"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="65"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="67"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="69"/>
        <source>Corporation</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="71"/>
        <source>Availability</source>
        <translation>Verfügbarkeit</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="73"/>
        <source>Issued</source>
        <translation>Ausgestellt</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="75"/>
        <source>Expiration</source>
        <translation>Ablauf</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="77"/>
        <source>Accepted</source>
        <translation>Angenommen</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="79"/>
        <location filename="../ContractModel.cpp" line="274"/>
        <source>Completed</source>
        <translation>Abgeschlossen</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="81"/>
        <source>Days</source>
        <translation>Tage</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="83"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="85"/>
        <source>Reward</source>
        <translation>Belohnung</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="87"/>
        <source>Collateral</source>
        <translation>Pfand</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="89"/>
        <source>Buyout</source>
        <translation>Sofortkauf</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="91"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="260"/>
        <source>Item Exchange</source>
        <translation>Gegenstandstausch</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="262"/>
        <source>Courier</source>
        <translation>Kurier</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="264"/>
        <source>Auction</source>
        <translation>Auktion</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="270"/>
        <source>Outstanding</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="272"/>
        <source>Deleted</source>
        <translation>Gelöscht</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="276"/>
        <source>Failed</source>
        <translation>Fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="278"/>
        <source>Completed by Issuer</source>
        <translation>Abgeschlossen durch Aussteller</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="280"/>
        <source>Completed by Contractor</source>
        <translation>Abgeschlossen durch Auftragnehmer</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="282"/>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="284"/>
        <source>Rejected</source>
        <translation>Abgelehnt</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="286"/>
        <source>Reversed</source>
        <translation>Aufgehoben</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="288"/>
        <source>In Progress</source>
        <translation>In Bearbeitung</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="294"/>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="294"/>
        <source>no</source>
        <translation>nein</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="298"/>
        <source>Private</source>
        <translation>Privat</translation>
    </message>
    <message>
        <location filename="../ContractModel.cpp" line="300"/>
        <source>Public</source>
        <translation>Öffentlich</translation>
    </message>
</context>
<context>
    <name>Evernus::ContractStatusesWidget</name>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="37"/>
        <source>Outstanding</source>
        <translation>Ausstehend (O)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="38"/>
        <source>Deleted</source>
        <translation>Gelöscht (D)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="39"/>
        <source>Completed</source>
        <translation>Abgeschlossen (C)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="40"/>
        <source>Failed</source>
        <translation>Fehlgeschlagen (F)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="41"/>
        <source>Completed by Issuer</source>
        <translation>Ausgeführt durch Aussteller (Ci)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="42"/>
        <source>Completed by Contractor</source>
        <translation>Ausgeführt durch Auftragnehmer (Cc)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="43"/>
        <source>Cancelled</source>
        <translation>Abgebrochen (Ca)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="44"/>
        <source>Rejected</source>
        <translation>Abgelehnt (Rj)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="45"/>
        <source>Reversed</source>
        <translation>Aufgehoben (Re)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="46"/>
        <source>In Progress</source>
        <translation>In Bearbeitung (Ip)</translation>
    </message>
    <message>
        <location filename="../ContractStatusesWidget.cpp" line="48"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>Evernus::ContractView</name>
    <message>
        <location filename="../ContractView.cpp" line="40"/>
        <source>Total contracts:</source>
        <translation>Verträge gesamt:</translation>
    </message>
    <message>
        <location filename="../ContractView.cpp" line="49"/>
        <source>Total price:</source>
        <translation>Preis gesamt:</translation>
    </message>
    <message>
        <location filename="../ContractView.cpp" line="55"/>
        <source>Total reward:</source>
        <translation>Belohnung gesamt:</translation>
    </message>
    <message>
        <location filename="../ContractView.cpp" line="61"/>
        <source>Total collateral:</source>
        <translation>Pfand gesamt:</translation>
    </message>
    <message>
        <location filename="../ContractView.cpp" line="67"/>
        <source>Total volume:</source>
        <translation>Volumen gesamt:</translation>
    </message>
</context>
<context>
    <name>Evernus::ContractWidget</name>
    <message>
        <location filename="../ContractWidget.cpp" line="82"/>
        <source>Issued</source>
        <translation>Ausgestellt</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="89"/>
        <source>Assigned</source>
        <translation>Angenommen</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="120"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="122"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="124"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="126"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="128"/>
        <source>Ci</source>
        <translation>Ci</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="130"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="132"/>
        <source>Ca</source>
        <translation>Ca</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="134"/>
        <source>Rj</source>
        <translation>Rj</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="136"/>
        <source>Re</source>
        <translation>Re</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="138"/>
        <source>Ip</source>
        <translation>Ip</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="140"/>
        <source>Status filter</source>
        <translation>Statusfilter</translation>
    </message>
    <message>
        <location filename="../ContractWidget.cpp" line="140"/>
        <source>Status filter [%1]  </source>
        <translation>Statusfilter [%1]</translation>
    </message>
</context>
<context>
    <name>Evernus::CorpImportPreferencesWidget</name>
    <message>
        <location filename="../CorpImportPreferencesWidget.cpp" line="33"/>
        <source>Corporation data import</source>
        <translation>Corporation Datenimport</translation>
    </message>
    <message>
        <location filename="../CorpImportPreferencesWidget.cpp" line="38"/>
        <source>Import corporation data along with character</source>
        <translation>Importiere Corporation Daten zusammen mit Charakter</translation>
    </message>
    <message>
        <location filename="../CorpImportPreferencesWidget.cpp" line="42"/>
        <source>Make value snapshots from corporation data</source>
        <translation>Erstelle Wertemomentaufnahme anhand Corporation Daten</translation>
    </message>
    <message>
        <source>Make value snapshots form corporation data</source>
        <translatorcomment>#TYPO# &quot;form&quot; -&gt; &quot;from&quot;</translatorcomment>
        <translation type="vanished">Erstelle Wertemomentaufnahme anhand Corporation Daten</translation>
    </message>
    <message>
        <location filename="../CorpImportPreferencesWidget.cpp" line="47"/>
        <source>Show corporation orders with character&apos;s</source>
        <translation>Zeige Corporation und Charakteraufträge zusammen</translation>
    </message>
</context>
<context>
    <name>Evernus::CorpKeyEditDialog</name>
    <message>
        <location filename="../CorpKeyEditDialog.cpp" line="44"/>
        <source>Character:</source>
        <translation>Charakter:</translation>
    </message>
    <message>
        <location filename="../CorpKeyEditDialog.cpp" line="62"/>
        <source>Key ID:</source>
        <translation>Key ID:</translation>
    </message>
    <message>
        <location filename="../CorpKeyEditDialog.cpp" line="66"/>
        <source>Verification Code:</source>
        <translation>Verification Code:</translation>
    </message>
    <message>
        <location filename="../CorpKeyEditDialog.cpp" line="68"/>
        <source>To create a predefined corporation key, use the following link:</source>
        <translation>Benutze diesen Link, um einen voreingestellten Corporation Key zu erzeugen:</translation>
    </message>
    <message>
        <location filename="../CorpKeyEditDialog.cpp" line="74"/>
        <source>Corporation keys require character keys added first.</source>
        <translation>Corporation Keys benötigen zuerst einen Charakter Key.</translation>
    </message>
    <message>
        <location filename="../CorpKeyEditDialog.cpp" line="81"/>
        <source>Corporation Key Edit</source>
        <translation>Corporation Key Bearbeiten</translation>
    </message>
    <message>
        <location filename="../CorpKeyEditDialog.cpp" line="89"/>
        <source>Invalid character</source>
        <translation>Ungültiger Charakter</translation>
    </message>
    <message>
        <location filename="../CorpKeyEditDialog.cpp" line="89"/>
        <source>Please select a valid character.</source>
        <translation>Bitte gültigen Charakter auswählen.</translation>
    </message>
</context>
<context>
    <name>Evernus::DateFilteredPlotWidget</name>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="39"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="46"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="53"/>
        <source>Show time labels</source>
        <translation>Zeiten anzeigen</translation>
    </message>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="58"/>
        <source>Show legend</source>
        <translation>Legende anzeigen</translation>
    </message>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="62"/>
        <source>Save...</source>
        <translation>Speichern...</translation>
    </message>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="117"/>
        <source>Save plot</source>
        <translation>Diagram speichern</translation>
    </message>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="117"/>
        <source>Images (*.png *.jpg *.jpeg *.bmp *.ppm *.xbm *.xpm)</source>
        <translation>Bilder (*.png *.jpg *jpeg *.bmp *.ppm *.xbm *.xpm)</translation>
    </message>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="122"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../DateFilteredPlotWidget.cpp" line="122"/>
        <source>Error saving image.</source>
        <translation>Fehler beim Speichern des Bilds.</translation>
    </message>
</context>
<context>
    <name>Evernus::DateRangeWidget</name>
    <message>
        <location filename="../DateRangeWidget.cpp" line="31"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location filename="../DateRangeWidget.cpp" line="38"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location filename="../DateRangeWidget.cpp" line="45"/>
        <source>Quick date  </source>
        <translatorcomment>#SPACE#</translatorcomment>
        <translation>Schnellauswahl</translation>
    </message>
    <message>
        <location filename="../DateRangeWidget.cpp" line="50"/>
        <source>Today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="../DateRangeWidget.cpp" line="56"/>
        <source>Past day</source>
        <translatorcomment>#NOTE# Wouldn&apos;t &quot;Yesterday&quot; be the better English term? I&apos;ve translated it that way. If you prefer &quot;Past day&quot; than &quot;Vorheriger Tag&quot; is the correct traslation</translatorcomment>
        <translation>Gestern</translation>
    </message>
    <message>
        <location filename="../DateRangeWidget.cpp" line="62"/>
        <source>This week</source>
        <translation>Diese Woche</translation>
    </message>
    <message>
        <location filename="../DateRangeWidget.cpp" line="69"/>
        <source>Past week</source>
        <translation>Letzte Woche</translation>
    </message>
    <message>
        <location filename="../DateRangeWidget.cpp" line="77"/>
        <source>This month</source>
        <translation>Dieser Monat</translation>
    </message>
    <message>
        <location filename="../DateRangeWidget.cpp" line="84"/>
        <source>Past month</source>
        <translation>Letzter Monat</translation>
    </message>
</context>
<context>
    <name>Evernus::DeviationSourceWidget</name>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="31"/>
        <source>Calculate deviation from:</source>
        <translation>Berechene Abweichung von:</translation>
    </message>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="33"/>
        <source>Median price</source>
        <translation>Durchschnittspreis</translation>
    </message>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="38"/>
        <source>Uses median price of displayed orders as reference.</source>
        <translation>Durchschnittspreis der angezeigten Aufträge als Referenz verwenden.</translation>
    </message>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="40"/>
        <source>Best buy/sell price</source>
        <translation>Bester Buy/Sell Preis</translation>
    </message>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="42"/>
        <source>Uses lowest sell order for buy orders or highest buy order for sell orders in the order station as reference.</source>
        <translation>Verwendet den niedriegsten Sell für Buy-Aufträge oder den höchsten Buy für Sell Auftäge in der Station des Auftrags als Referenz.</translation>
    </message>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="44"/>
        <source>Custom cost</source>
        <translation>Benutzerdefinierte Kosten</translation>
    </message>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="46"/>
        <source>Uses custom item cost as reference.</source>
        <translation>Verwendet benutzerdefinierte Kosten als Referenz.</translation>
    </message>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="48"/>
        <source>Fixed price</source>
        <translation>Fester Preis</translation>
    </message>
    <message>
        <location filename="../DeviationSourceWidget.cpp" line="50"/>
        <source>Uses given fixed value as reference.</source>
        <translation>Verwendet angegebenen festen Preis als Referenz.</translation>
    </message>
</context>
<context>
    <name>Evernus::EvernusApplication</name>
    <message>
        <location filename="../EvernusApplication.cpp" line="114"/>
        <location filename="../EvernusApplication.cpp" line="201"/>
        <source>Loading...</source>
        <translation>Lade...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="116"/>
        <source>Creating databases...</source>
        <translation>Erzeuge Datenbanken...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="119"/>
        <source>Creating schemas...</source>
        <translation>Erzeuge Schemata...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="147"/>
        <source>Precaching ref types...</source>
        <translation>Erzeuge Zwischenspeicher Ref Types...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="150"/>
        <source>Precaching timers...</source>
        <translation>Puffere Timer...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="154"/>
        <source>Precaching jump map...</source>
        <translation>Puffere Jump Map...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="157"/>
        <source>Clearing old wallet entries...</source>
        <translation>Lösche alte Walleteinträge...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="160"/>
        <source>Clearing old market orders...</source>
        <translation>Lösche alte Marktaufträge...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="163"/>
        <source>Setting up IGB service...</source>
        <translation>Richte den IGB Dienst ein...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="184"/>
        <source>Setting up HTTP service...</source>
        <translation>Richte den HTTP Dienst ein...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="326"/>
        <location filename="../EvernusApplication.cpp" line="372"/>
        <source>Unknown cache timer type: %1</source>
        <translation>Unbekannter Cache Timer Typ: %1</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="442"/>
        <source>Unknown update timer type: %1</source>
        <translation>Unbekannter Update Timer Typ: %1</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="757"/>
        <source>Fetching characters...</source>
        <translation>Hole Charaktere...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="770"/>
        <source>Fetching characters for key %1...</source>
        <translation>Hole Charactere für Key %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="783"/>
        <location filename="../EvernusApplication.cpp" line="789"/>
        <location filename="../EvernusApplication.cpp" line="2229"/>
        <location filename="../EvernusApplication.cpp" line="2267"/>
        <source>Evernus</source>
        <translation>Evernus</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="783"/>
        <source>An error occurred while updating character key information: %1. Data sync should work, but character tab will display incorrect information.</source>
        <translation>Beim Aktualiseren der Informationen zum Charakter Key ist ein Fehler aufgetreten: %1. Die Synchronisation sollte funktionieren, aber das Register Charakter wird falsche Daten anzeigen.</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="789"/>
        <source>An error occurred while updating character key information. Data sync should work, but character tab will display incorrect information.</source>
        <translation>Beim Aktualiseren der Informationen zum Charakter Key ist ein Fehler aufgetreten. Die Synchronisation sollte funktionieren, aber das Register Charakter wird falsche Daten anzeigen.</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="838"/>
        <source>Fetching assets for character %1...</source>
        <translation>Hole Gegenstände für Charakter %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="873"/>
        <location filename="../EvernusApplication.cpp" line="933"/>
        <location filename="../EvernusApplication.cpp" line="994"/>
        <location filename="../EvernusApplication.cpp" line="1050"/>
        <location filename="../EvernusApplication.cpp" line="1075"/>
        <location filename="../EvernusApplication.cpp" line="1142"/>
        <location filename="../EvernusApplication.cpp" line="1202"/>
        <location filename="../EvernusApplication.cpp" line="1259"/>
        <location filename="../EvernusApplication.cpp" line="1284"/>
        <source>Key not found!</source>
        <translation>Key nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="877"/>
        <location filename="../EvernusApplication.cpp" line="937"/>
        <location filename="../EvernusApplication.cpp" line="998"/>
        <location filename="../EvernusApplication.cpp" line="1054"/>
        <location filename="../EvernusApplication.cpp" line="1079"/>
        <location filename="../EvernusApplication.cpp" line="1206"/>
        <location filename="../EvernusApplication.cpp" line="1263"/>
        <source>Character not found!</source>
        <translation>Charakter nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="885"/>
        <source>Fetching contracts for character %1...</source>
        <translation>Hole Verträge für Character %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="945"/>
        <source>Fetching wallet journal for character %1...</source>
        <translation>Hole Konto-Logbuch für Charakter %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="954"/>
        <source>Fetching wallet journal for character %1 (this may take a while)...</source>
        <translation>Hole Konto-Logbuch für Charakter %1 (dies kann eine Weile dauern)...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1006"/>
        <source>Fetching wallet transactions for character %1...</source>
        <translation>Hole Konto-Transaktionen für Charakter %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1018"/>
        <source>Fetching wallet transactions for character %1 (this may take a while)...</source>
        <translation>Hole Konto-Transaktionen für Charakter %1 (dies kann eine Weile dauern)...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1062"/>
        <location filename="../EvernusApplication.cpp" line="1087"/>
        <source>Fetching market orders for character %1...</source>
        <translation>Hole Marktaufträge für Charakter %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1099"/>
        <source>Fetching corporation contracts for character %1...</source>
        <translation>Hole Verträge für Corporation %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1150"/>
        <source>Fetching corporation wallet journal for character %1...</source>
        <translation>Hole Corporation Konto-Logbuch für Charakter %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1162"/>
        <source>Fetching corporation wallet journal for character %1 (this may take a while)...</source>
        <translation>Hiole Corporation Konto-Logbuch für Charakter %1 (dies kann eine Weile dauern)...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1214"/>
        <source>Fetching corporation wallet transactions for character %1...</source>
        <translation>Hole Corporation Konto-Transaktionen für Charakter %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1227"/>
        <source>Fetching corporation wallet transactions for character %1 (this may take a while)...</source>
        <translation>Hole Corporation Konto-Transaktionen für Charakter %1 (dies kann eine Weile dauern)...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1271"/>
        <location filename="../EvernusApplication.cpp" line="1292"/>
        <source>Fetching corporation market orders for character %1...</source>
        <translation>Hole Corporation Marktaufträge für Charakter %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1304"/>
        <source>Fetching conquerable stations...</source>
        <translation>Hole Stationsliste...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1368"/>
        <source>Saving %1 imported orders...</source>
        <translation>Speichere %1 importierte Aufträge...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1423"/>
        <source>Select Mentat directory</source>
        <translation>Wäher Mentat-Verzeichnis aus</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1433"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1433"/>
        <source>Error opening %1</source>
        <translation>Fehler beim Öffnen von %1</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1437"/>
        <source>Importing order history...</source>
        <translation>Importiere Auftragshistorie...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1483"/>
        <source>Importing order history: %1 processed</source>
        <translation>Importiere Auftragshistorie: %1 verarbeitet</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1488"/>
        <source>Importing order history: storing %1 orders (this may take a while)</source>
        <translatorcomment>#NOTE# Seems you&apos;re missing the &quot;...&quot; at the end of this phrase, denoting a time-consuming activity like you&apos;ve did with similar phrases.</translatorcomment>
        <translation>Importiere Auftragshistorie: speichere %1 Aufträge (dies kann eine Weile dauern)...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1509"/>
        <source>Synchronizing with LMeve...</source>
        <translation>Synchronisiere mit LMeve...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1602"/>
        <source>SMTP Error</source>
        <translation>SMTP Fehler</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1602"/>
        <location filename="../EvernusApplication.cpp" line="1610"/>
        <source>Error sending email: %1</source>
        <translation>Fehler beim Emailversnad: %1</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1610"/>
        <source>Mail Error</source>
        <translation>Emailfehler</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1915"/>
        <source>Importing item prices...</source>
        <translation>Importiere Warenpreise...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1926"/>
        <source>Cannot determine market logs path!</source>
        <translation>Verzeichnis mit Mark Logs nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1940"/>
        <source>No market logs found!</source>
        <translation>Keine Mark Logs gefunden!</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="1951"/>
        <source>Could not open market log file!</source>
        <translation>Markt Log konnte nicht geöffnet werden!</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="2230"/>
        <source>[Evernus] Market orders fulfilled</source>
        <translation>[Evernus] Ausgeführte Marktaufträge</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="2234"/>
        <source>The following orders have changed their status:

</source>
        <translation>Der Status der folgenden Aufträge hat sich geändert:

</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="2237"/>
        <source>    %1 x%2 [%3]
</source>
        <translation>    %1 x %2 [%3]
</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="2267"/>
        <source>Couldn&apos;t find character for order import!</source>
        <translation>Charakter für Auftragsimport nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="2465"/>
        <source>Fetching contract items for contract %1...</source>
        <translation>Hole Vertragsgegenstände für Vertrag %1...</translation>
    </message>
    <message>
        <location filename="../EvernusApplication.cpp" line="2539"/>
        <source>Fetching character %1...</source>
        <translation>Hole Charakter %1...</translation>
    </message>
</context>
<context>
    <name>Evernus::ExternalOrderBuyModel</name>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="83"/>
        <location filename="../ExternalOrderBuyModel.cpp" line="114"/>
        <location filename="../ExternalOrderBuyModel.cpp" line="418"/>
        <source>Station</source>
        <translation>Station</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="85"/>
        <source>Deviation</source>
        <translation>Abweichung</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="87"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="89"/>
        <location filename="../ExternalOrderBuyModel.cpp" line="130"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="91"/>
        <source>Range</source>
        <translation>Reichweite</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="93"/>
        <source>Min. quantity</source>
        <translation>Mindestmenge</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="95"/>
        <location filename="../ExternalOrderBuyModel.cpp" line="132"/>
        <source>Total cost</source>
        <translation>Kosten gesamt</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="97"/>
        <location filename="../ExternalOrderBuyModel.cpp" line="136"/>
        <source>Total size</source>
        <translation>Größe gesamt</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="99"/>
        <source>Issued</source>
        <translation>Erstellt</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="101"/>
        <source>Time left</source>
        <translation>Restlaufzeit</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="103"/>
        <source>Imported</source>
        <translation>Importiert</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="105"/>
        <location filename="../ExternalOrderBuyModel.cpp" line="118"/>
        <location filename="../ExternalOrderBuyModel.cpp" line="422"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="116"/>
        <source>Solar system</source>
        <translation>Sternensystem</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="124"/>
        <source>Lowest price</source>
        <translation>Niedrigster Preis</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="126"/>
        <source>Median price</source>
        <translation>Durchschnittspreis</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="128"/>
        <source>Highest price</source>
        <translation>Höchster Preis</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="134"/>
        <source>Orders</source>
        <translation>Aufträge</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="420"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="424"/>
        <source>%1 jumps</source>
        <translation>%1 Sprünge</translation>
    </message>
    <message>
        <location filename="../ExternalOrderBuyModel.cpp" line="501"/>
        <source>Your order</source>
        <translation>Dein Auftrag</translation>
    </message>
</context>
<context>
    <name>Evernus::ExternalOrderSellModel</name>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="83"/>
        <location filename="../ExternalOrderSellModel.cpp" line="110"/>
        <source>Station</source>
        <translation>Station</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="85"/>
        <source>Deviation</source>
        <translation>Abweichung</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="87"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="89"/>
        <location filename="../ExternalOrderSellModel.cpp" line="126"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="91"/>
        <location filename="../ExternalOrderSellModel.cpp" line="128"/>
        <source>Total profit</source>
        <translation>Profit gesamt</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="93"/>
        <location filename="../ExternalOrderSellModel.cpp" line="132"/>
        <source>Total size</source>
        <translation>Größe gesamt</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="95"/>
        <source>Issued</source>
        <translation>Erstellt</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="97"/>
        <source>Time left</source>
        <translation>Restlaufzeit</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="99"/>
        <source>Imported</source>
        <translation>Importiert</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="101"/>
        <location filename="../ExternalOrderSellModel.cpp" line="114"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="112"/>
        <source>Solar system</source>
        <translation>Sternensystem</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="120"/>
        <source>Lowest price</source>
        <translation>Niedrigster Preis</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="122"/>
        <source>Median price</source>
        <translation>Durchschnittspreis</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="124"/>
        <source>Highest price</source>
        <translation>Höchster Preis</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="130"/>
        <source>Orders</source>
        <translation>Aufträge</translation>
    </message>
    <message>
        <location filename="../ExternalOrderSellModel.cpp" line="477"/>
        <source>Your order</source>
        <translation>Dein Auftrag</translation>
    </message>
</context>
<context>
    <name>Evernus::ExternalOrderView</name>
    <message>
        <location filename="../ExternalOrderView.cpp" line="70"/>
        <source>Total cost:</source>
        <translation>Kosten gesamt:</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="76"/>
        <source>Total volume:</source>
        <translation>Volumen gesamt:</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="82"/>
        <source>Total size:</source>
        <translation>Größe gesamt:</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="88"/>
        <source>Min. price:</source>
        <translation>Tiefstpreis:</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="94"/>
        <source>Median price:</source>
        <translation>Durchschnittspreis:</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="100"/>
        <source>Max. price:</source>
        <translation>Höchstpreis:</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="106"/>
        <source>Custom cost:</source>
        <translation>Benutzerdefinierte Kosten:</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="195"/>
        <source>Copy suggested price: %1</source>
        <translation>Vorgeschlagegen Preis kopieren: %1</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="211"/>
        <location filename="../ExternalOrderView.cpp" line="217"/>
        <source>N/A</source>
        <translatorcomment>#NOTE# Although Linguist complains about missing interpunktion, this is the equivalent German abbreviation.</translatorcomment>
        <translation>k.A.</translation>
    </message>
    <message>
        <location filename="../ExternalOrderView.cpp" line="240"/>
        <source>Copy suggested price</source>
        <translation>Vorgeschlagegen Preis kopieren</translation>
    </message>
</context>
<context>
    <name>Evernus::FileDownload</name>
    <message>
        <location filename="../FileDownload.cpp" line="33"/>
        <source>Error creating file: %1</source>
        <translation>Fehler beim Erzeugen der Datei %1</translation>
    </message>
</context>
<context>
    <name>Evernus::GeneralPreferencesWidget</name>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="46"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="53"/>
        <source>Language changes require application restart.</source>
        <translation>Eine Änderung der Sprache erfordert einen Neustart der Anwendung.</translation>
    </message>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="62"/>
        <source>Minimize to tray</source>
        <translation>In den Nachrichtenbereich minimieren</translation>
    </message>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="66"/>
        <source>Minimize when opening the Margin Tool</source>
        <translation>Beim Öffnen des Margenrechners minimieren</translation>
    </message>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="70"/>
        <source>Check for updates on startup</source>
        <translation>Beim Starten auf Updates prüfen</translation>
    </message>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="74"/>
        <source>Use packaged size for ships</source>
        <translation>Benutze verpackte Größe für Schiffe</translation>
    </message>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="78"/>
        <source>Omit currency symbol (requires restart)</source>
        <translation>Währungssymbol ausblenden (Benötigt Neustart)</translation>
    </message>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="82"/>
        <source>Force UTC date/time (requires restart)</source>
        <translation>UTC Zeit verwenden (Benötigt Neustart)</translation>
    </message>
    <message>
        <location filename="../GeneralPreferencesWidget.cpp" line="89"/>
        <source>Date/time format (requires restart):</source>
        <translatorcomment>#NOTE# As programmers, we are familiar with date format strings. But perhaps &quot;Joe User&quot; would appreciate some hints here as to what abbriviations he can use.</translatorcomment>
        <translation>Datums-/Zeitformat (Benötigt Neutstart):</translation>
    </message>
</context>
<context>
    <name>Evernus::GenericMarketOrdersInfoWidget</name>
    <message>
        <location filename="../GenericMarketOrdersInfoWidget.cpp" line="36"/>
        <source>Active orders:</source>
        <translation>Aktive Auträge:</translation>
    </message>
    <message>
        <location filename="../GenericMarketOrdersInfoWidget.cpp" line="42"/>
        <source>Total volume:</source>
        <translation>Volumen gesamt:</translation>
    </message>
    <message>
        <location filename="../GenericMarketOrdersInfoWidget.cpp" line="48"/>
        <source>Total ISK in orders:</source>
        <translation>Gesamtwert Aufträge:</translation>
    </message>
    <message>
        <location filename="../GenericMarketOrdersInfoWidget.cpp" line="54"/>
        <source>Total size:</source>
        <translation>Gesamtgröße:</translation>
    </message>
</context>
<context>
    <name>Evernus::HttpPreferencesWidget</name>
    <message>
        <location filename="../HttpPreferencesWidget.cpp" line="45"/>
        <source>Enabled</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <location filename="../HttpPreferencesWidget.cpp" line="50"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../HttpPreferencesWidget.cpp" line="56"/>
        <source>HTTP user:</source>
        <translation>HTTP User:</translation>
    </message>
    <message>
        <location filename="../HttpPreferencesWidget.cpp" line="59"/>
        <source>HTTP password:</source>
        <translation>HTTP Paßwort:</translation>
    </message>
    <message>
        <location filename="../HttpPreferencesWidget.cpp" line="62"/>
        <source>Warning: password store uses weak encryption - do not use sensitive passwords.</source>
        <translation>Warnung: Paßwörter werden mit einer schwacher Verschlüsselung gespeichert - benutze keine sensiblen Paßwörter.</translation>
    </message>
</context>
<context>
    <name>Evernus::HttpService</name>
    <message>
        <location filename="../HttpService.cpp" line="61"/>
        <source>Select Character</source>
        <translation>Charakter wählen</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="62"/>
        <source>Character Orders</source>
        <translation>Aufträge Charakter</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="63"/>
        <source>Corporation Orders</source>
        <translation>Corporation Aufträge</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="65"/>
        <source>Select character:</source>
        <translation>Charakter wählen:</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="66"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="69"/>
        <source>Filters</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="70"/>
        <source>Status filter:</source>
        <translation>Statusfilter:</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="72"/>
        <source>Changed</source>
        <translation>Geändert</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="74"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="76"/>
        <source>Fulfilled</source>
        <translation>Ausgeführt</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="78"/>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="80"/>
        <source>Pending</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="82"/>
        <source>Deleted</source>
        <translation>Gelöscht</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="84"/>
        <source>Expired</source>
        <translation>Abgelaufen</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="85"/>
        <source>Price status filter:</source>
        <translation>Preisstatusfilter:</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="87"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="89"/>
        <source>No data</source>
        <translation>Keine Daten</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="91"/>
        <source>Data too old</source>
        <translation>Daten zu alt</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="92"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="93"/>
        <source>Sell Orders</source>
        <translation>Verkaufsaufträge</translation>
    </message>
    <message>
        <location filename="../HttpService.cpp" line="94"/>
        <source>Buy Orders</source>
        <translation>Kaufaufträge</translation>
    </message>
</context>
<context>
    <name>Evernus::IGBPreferencesWidget</name>
    <message>
        <location filename="../IGBPreferencesWidget.cpp" line="43"/>
        <source>Enabled</source>
        <translation>Aktiviert</translation>
    </message>
    <message>
        <location filename="../IGBPreferencesWidget.cpp" line="48"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
</context>
<context>
    <name>Evernus::IGBService</name>
    <message>
        <location filename="../IGBService.cpp" line="69"/>
        <source>Error reading IGB script template.</source>
        <translation>Fehler beim Einlesen der IGB-Skriptvorlage.</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="73"/>
        <source>Character Active Orders</source>
        <translation>Charakter Aktive Aufträge</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="74"/>
        <source>Character Fulfilled Orders</source>
        <translation>Charakter Ausgeführte Aufträge</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="75"/>
        <source>Character Overbid Orders</source>
        <translation>Charakter Überbotene Aufträge</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="76"/>
        <source>Character Below Min. Margin Orders</source>
        <translation>Charakter Aufträge unter Mindestmarge</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="77"/>
        <source>Corporation Active Orders</source>
        <translation>Corporation Aktive Aufträge</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="78"/>
        <source>Corporation Fulfilled Orders</source>
        <translation>Corporation Ausgeführte Aufträge</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="79"/>
        <source>Corporation Overbid Orders</source>
        <translation>Corporation Überbotene Aufträge</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="80"/>
        <source>Corporation Below Min. Margin Orders</source>
        <translation>Corporation Aufträge unter Mindestmarge</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="81"/>
        <source>Favorite Items</source>
        <translation>Favoritenartikel</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="82"/>
        <source>Open Margin Tool</source>
        <translation>Margenrechner öffnen</translation>
    </message>
    <message>
        <source>Trade Advisor</source>
        <translation type="vanished">Handelsberater</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="88"/>
        <source>Show Previous Entry</source>
        <translation>Zeige vorherigen Eintrag</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="89"/>
        <source>Show Next Entry</source>
        <translation>Zeige nächsten Eintrag</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="90"/>
        <source>Current entry:</source>
        <translation>Aktueller Eintrag:</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="93"/>
        <source>Favorite items:</source>
        <translation>Favoriten:</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="95"/>
        <source>Sell orders:</source>
        <translation>Verkaufsaufträge:</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="96"/>
        <source>Buy orders:</source>
        <translation>Kaufaufträge:</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="97"/>
        <source>Limit to current station, if available</source>
        <translation>Auf diese Station begrenzen, falls möglich</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="103"/>
        <source>Please select a category from the menu.</source>
        <translation>Bitte eine Kategorie aus dem Menü auswählen.</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="208"/>
        <location filename="../IGBService.cpp" line="314"/>
        <source>Character not found!</source>
        <translation>Charakter nicht gefunden!</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="370"/>
        <source>Website trust is required. Please add it to Trusted Sites.</source>
        <translation>Bitte füge die Webseite der Liste der vertrauten Seiten hinzu.</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="456"/>
        <source>Evernus</source>
        <translation>Evernus</translation>
    </message>
    <message>
        <location filename="../IGBService.cpp" line="456"/>
        <source>You have to open Evernus in EVE In-Game Browser first.</source>
        <translation>Du mußt Evernus zuerst im EVE-Browser öffnen.</translation>
    </message>
</context>
<context>
    <name>Evernus::ImportPreferencesWidget</name>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="44"/>
        <source>Ignore up-to-date data</source>
        <translation>Aktuelle Daten nicht erneut importieren</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="48"/>
        <source>Import data for all characters with one click</source>
        <translation>Alle Daten aller Charaktere mit einem Klick importieren</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="52"/>
        <source>Data age</source>
        <translation>Alter der Daten</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="58"/>
        <source>Max. character update age:</source>
        <translation>Max. Updatealter Charakter:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="61"/>
        <source>Max. asset list update age:</source>
        <translation>Max. Updatealter Gegenstände:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="64"/>
        <source>Max. wallet update age:</source>
        <translation>Max. Updatealter Kontodaten:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="67"/>
        <source>Max. market orders update age:</source>
        <translation>Max. Updatealter Marktaufträge:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="70"/>
        <source>Max. contracts update age:</source>
        <translation>Max. Updatealter Verträge:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="72"/>
        <source>Auto-import</source>
        <translation>Automatischer Import</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="77"/>
        <source>Enabled</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="82"/>
        <source>Auto-import time:</source>
        <translation>Updateintervall:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="85"/>
        <location filename="../ImportPreferencesWidget.cpp" line="153"/>
        <source>min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="88"/>
        <source>Enable email notifications</source>
        <translation>Emailbenachrichtigung aktivieren</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="94"/>
        <source>Destination address:</source>
        <translation>Zeiladresse:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="100"/>
        <source>SMTP security:</source>
        <translation>SMTP Sicherheit:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="101"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="102"/>
        <source>STARTTLS</source>
        <translation>STARTTLS</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="103"/>
        <source>SSL/TLS</source>
        <translation>SSL/TLS</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="106"/>
        <source>SMTP host:</source>
        <translation>SMTP Host:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="109"/>
        <source>SMTP port:</source>
        <translation>SMTP Port:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="115"/>
        <source>SMTP user:</source>
        <translation>SMTP Benutzer:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="118"/>
        <source>SMTP password:</source>
        <translation>SMTP Paßwort:</translation>
    </message>
    <message>
        <location filename="../ImportPreferencesWidget.cpp" line="121"/>
        <source>Warning: password store uses weak encryption - do not use sensitive passwords.</source>
        <translation>Warnung: Paßwörter werden mit einer schwacher Verschlüsselung gespeichert - benutze keine sensiblen Paßwörter.</translation>
    </message>
</context>
<context>
    <name>Evernus::ImportSourcePreferencesWidget</name>
    <message>
        <location filename="../ImportSourcePreferencesWidget.cpp" line="32"/>
        <source>Default import sources</source>
        <translation>Standardquellen für Datenimport</translation>
    </message>
    <message>
        <location filename="../ImportSourcePreferencesWidget.cpp" line="40"/>
        <source>Prices:</source>
        <translation>Preise:</translation>
    </message>
    <message>
        <location filename="../ImportSourcePreferencesWidget.cpp" line="45"/>
        <source>Web</source>
        <translation>Web</translation>
    </message>
    <message>
        <location filename="../ImportSourcePreferencesWidget.cpp" line="46"/>
        <location filename="../ImportSourcePreferencesWidget.cpp" line="55"/>
        <source>Logs</source>
        <translation>Logs</translation>
    </message>
    <message>
        <location filename="../ImportSourcePreferencesWidget.cpp" line="49"/>
        <source>Market orders:</source>
        <translation>Marktaufträge:</translation>
    </message>
    <message>
        <location filename="../ImportSourcePreferencesWidget.cpp" line="54"/>
        <source>API</source>
        <translation>API</translation>
    </message>
</context>
<context>
    <name>Evernus::InterRegionMarketDataModel</name>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="123"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="125"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="127"/>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="129"/>
        <source>5% volume source price (b/s)</source>
        <translation>5% Volumen Quellpreis (b/s)</translation>
    </message>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="133"/>
        <source>5% volume destination price (b/s)</source>
        <translation>5% Volumen Zielpreis (b/s)</translation>
    </message>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="135"/>
        <source>Best difference</source>
        <translation>Beste Differenz</translation>
    </message>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="137"/>
        <source>30-day avg. min. volume</source>
        <translation>30 Tage Durchschnitt Min. Volumen</translation>
    </message>
    <message>
        <source>5% volume buy price</source>
        <translation type="obsolete">5% Spanne Kaufpreis</translation>
    </message>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="131"/>
        <source>Destination</source>
        <translation>Ziel</translation>
    </message>
    <message>
        <source>5% volume sell price</source>
        <translation type="obsolete">5% Spanne Verkaufspreis</translation>
    </message>
    <message>
        <source>Difference</source>
        <translation type="obsolete">Differenz</translation>
    </message>
    <message>
        <source>30-day avg. volume</source>
        <translation type="obsolete">30 Tage Durchschnittsvolumen</translation>
    </message>
    <message>
        <location filename="../InterRegionMarketDataModel.cpp" line="139"/>
        <source>Margin</source>
        <translation>Marge</translation>
    </message>
</context>
<context>
    <name>Evernus::ItemCostEditDialog</name>
    <message>
        <location filename="../ItemCostEditDialog.cpp" line="55"/>
        <source>Cost:</source>
        <translation>Kosten:</translation>
    </message>
    <message>
        <location filename="../ItemCostEditDialog.cpp" line="70"/>
        <source>Item Cost Edit</source>
        <translation>Kosten bearbeiten</translation>
    </message>
</context>
<context>
    <name>Evernus::ItemCostModel</name>
    <message>
        <location filename="../ItemCostModel.cpp" line="39"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../ItemCostModel.cpp" line="41"/>
        <source>Cost</source>
        <translation>Kosten</translation>
    </message>
</context>
<context>
    <name>Evernus::ItemCostWidget</name>
    <message>
        <location filename="../ItemCostWidget.cpp" line="42"/>
        <source>Add...</source>
        <translation>Hinzufügen...</translation>
    </message>
    <message>
        <location filename="../ItemCostWidget.cpp" line="47"/>
        <source>Edit...</source>
        <translation>Bearbeiten...</translation>
    </message>
    <message>
        <location filename="../ItemCostWidget.cpp" line="53"/>
        <source>Remove</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../ItemCostWidget.cpp" line="59"/>
        <source>Remove all</source>
        <translation>Alle löschen</translation>
    </message>
    <message>
        <location filename="../ItemCostWidget.cpp" line="66"/>
        <source>type in wildcard and press Enter</source>
        <translation>Begriff eingeben und ENTER drücken</translation>
    </message>
</context>
<context>
    <name>Evernus::ItemHistoryWidget</name>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="54"/>
        <source>Item type:</source>
        <translation>Artikeltyp:</translation>
    </message>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="63"/>
        <source>Combine for all characters</source>
        <translation>Für alle Charaktere zusammenfassen</translation>
    </message>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="83"/>
        <location filename="../ItemHistoryWidget.cpp" line="99"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="105"/>
        <source>Balance</source>
        <translation>Differenz</translation>
    </message>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="116"/>
        <source>Total income:</source>
        <translation>Einkünfte gesamt:</translation>
    </message>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="122"/>
        <source>Total cost:</source>
        <translation>Kosten gesamt:</translation>
    </message>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="128"/>
        <source>Balance:</source>
        <translation>Kontostand:</translation>
    </message>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="134"/>
        <source>Margin:</source>
        <translation>Marge:</translation>
    </message>
    <message>
        <location filename="../ItemHistoryWidget.cpp" line="140"/>
        <source>Total volume:</source>
        <translation>Volumen gesamt:</translation>
    </message>
</context>
<context>
    <name>Evernus::ItemNameModel</name>
    <message>
        <location filename="../ItemNameModel.cpp" line="43"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
</context>
<context>
    <name>Evernus::ItemTypeSelectDialog</name>
    <message>
        <location filename="../ItemTypeSelectDialog.cpp" line="47"/>
        <source>Select Item Type</source>
        <translation>Artikeltyp auswählen</translation>
    </message>
</context>
<context>
    <name>Evernus::KeyEditDialog</name>
    <message>
        <location filename="../KeyEditDialog.cpp" line="43"/>
        <source>Key ID:</source>
        <translation>Key ID:</translation>
    </message>
    <message>
        <location filename="../KeyEditDialog.cpp" line="47"/>
        <source>Verification Code:</source>
        <translation>Verification Code:</translation>
    </message>
    <message>
        <location filename="../KeyEditDialog.cpp" line="49"/>
        <source>To create a predefined key, use the following link:</source>
        <translation>Benutze diesen Link, um einen voreingestellten Key zu erzeugen:</translation>
    </message>
    <message>
        <location filename="../KeyEditDialog.cpp" line="60"/>
        <source>Key Edit</source>
        <translation>Key bearbeiten</translation>
    </message>
</context>
<context>
    <name>Evernus::LMeveAPIInterface</name>
    <message>
        <location filename="../LMeveAPIInterface.cpp" line="60"/>
        <source>Missing LMeve url!</source>
        <translation>Fehlende LMeve URL!</translation>
    </message>
    <message>
        <location filename="../LMeveAPIInterface.cpp" line="67"/>
        <source>Missing LMeve key!</source>
        <translation>Fehlender LMeve Key!</translation>
    </message>
</context>
<context>
    <name>Evernus::LMeveAPIManager</name>
    <message>
        <location filename="../LMeveAPIManager.cpp" line="59"/>
        <source>Unexpected object received!</source>
        <translatorcomment>#NOTE# I suppose these are internat error messages and therefore not meant for public consumption, translation doesn&apos;t really make sense. Leave them &quot;as is&quot; or use &quot;Unerwartetes Objekt empfangen&quot;</translatorcomment>
        <translation>Unexpected object received!</translation>
    </message>
    <message>
        <location filename="../LMeveAPIManager.cpp" line="63"/>
        <source>Expected task array!</source>
        <translatorcomment>#NOTE# Leave &quot;as is&quot; or use &quot;Aufgabenmenge erwartet&quot;</translatorcomment>
        <translation>Expected task array!</translation>
    </message>
    <message>
        <location filename="../LMeveAPIManager.cpp" line="88"/>
        <source>Empty response from server!</source>
        <translatorcomment>#NOTE# Leave &quot;as is&quot; or use &quot;Leere Serverantwort erhalten!&quot;</translatorcomment>
        <translation>Empty response from server!</translation>
    </message>
</context>
<context>
    <name>Evernus::LMevePreferencesWidget</name>
    <message>
        <location filename="../LMevePreferencesWidget.cpp" line="40"/>
        <source>To start working with LMeve, visit its &lt;a href=&apos;https://github.com/roxlukas/lmeve&apos;&gt;homepage&lt;/a&gt;.</source>
        <translation>Um mit LMeve zu arbeiten, besuche seine &lt;a href=&apos;https://github.com/roxlukas/lmeve&apos;&gt;Homepage&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../LMevePreferencesWidget.cpp" line="48"/>
        <source>Url:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../LMevePreferencesWidget.cpp" line="51"/>
        <source>Key:</source>
        <translation>Key:</translation>
    </message>
</context>
<context>
    <name>Evernus::LMeveTaskModel</name>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="147"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="174"/>
        <source>Price data is too old (valid on %1).</source>
        <translation>Preisdaten sind zu alt (gültig am %1).</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="210"/>
        <source>Item</source>
        <translation>Artikel</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="212"/>
        <source>Activity</source>
        <translation>Aktivität</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="214"/>
        <source>Runs</source>
        <translation>Durchläufe</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="216"/>
        <source>Runs completed and in progress</source>
        <translation>Fertige und in Arbeit befindliche Durchläufe</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="218"/>
        <source>Runs completed</source>
        <translation>Fertige Durchläufe</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="220"/>
        <source>Jobs completed and in progress</source>
        <translation>Fertige und in Bearbeitung befindliche Aufträge</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="222"/>
        <source>Successful jobs</source>
        <translation>Erfolgreiche Aufträge</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="224"/>
        <source>Jobs completed</source>
        <translation>Fertige Aufträge</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="226"/>
        <source>Custom cost</source>
        <translation>Benutzerdefinierte Kosten</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="228"/>
        <source>Sell price</source>
        <translation>Verkaufspreis</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="230"/>
        <source>Profit</source>
        <translation>Profit</translation>
    </message>
    <message>
        <location filename="../LMeveTaskModel.cpp" line="232"/>
        <source>Margin</source>
        <translation>Marge</translation>
    </message>
</context>
<context>
    <name>Evernus::LMeveWidget</name>
    <message>
        <location filename="../LMeveWidget.cpp" line="52"/>
        <source>Synchronize</source>
        <translation>Synchronisiere</translation>
    </message>
    <message>
        <location filename="../LMeveWidget.cpp" line="60"/>
        <source>Before synchronizing, enter LMeve url and key in the &lt;a href=&apos;#&apos;&gt;Preferences&lt;/a&gt;.</source>
        <translation>Gib vor der Synchronisation die LMeve URL und den Key in &lt;a href=&apos;#&apos;&gt;Einstellungen&lt;/a&gt; an.</translation>
    </message>
    <message>
        <location filename="../LMeveWidget.cpp" line="69"/>
        <source>Tasks</source>
        <translation>Aufgaben</translation>
    </message>
    <message>
        <location filename="../LMeveWidget.cpp" line="126"/>
        <source>Sell station</source>
        <translation>Station Verkauf</translation>
    </message>
    <message>
        <location filename="../LMeveWidget.cpp" line="138"/>
        <source>Import prices from Web</source>
        <translation>Preisimport aus dem Web</translation>
    </message>
    <message>
        <location filename="../LMeveWidget.cpp" line="139"/>
        <source>Import prices from logs</source>
        <translation>Preisimport aus Logs</translation>
    </message>
    <message>
        <location filename="../LMeveWidget.cpp" line="141"/>
        <source>Import prices  </source>
        <translation>Preise importieren</translation>
    </message>
</context>
<context>
    <name>Evernus::LanguageSelectDialog</name>
    <message>
        <location filename="../LanguageSelectDialog.cpp" line="34"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../LanguageSelectDialog.cpp" line="44"/>
        <source>Select Language</source>
        <translation>Sprache wählen</translation>
    </message>
</context>
<context>
    <name>Evernus::LocationBookmarkSelectDialog</name>
    <message>
        <location filename="../LocationBookmarkSelectDialog.cpp" line="48"/>
        <source>Select Bookmark</source>
        <translation>Lesezeichen wählen</translation>
    </message>
</context>
<context>
    <name>Evernus::MainWindow</name>
    <message>
        <source>Error</source>
        <translation type="vanished">Fehler</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="205"/>
        <location filename="../MainWindow.cpp" line="449"/>
        <location filename="../MainWindow.cpp" line="471"/>
        <source>Evernus</source>
        <translation>Evernus</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="205"/>
        <source>You can show/hide table columns via right-click menu. Columns can also be moved around via dragging.</source>
        <translation>Spalten können via Rechtsklick angezeigt/verborgen werden. Sie können durch Ziehen auch verschoben werden.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="245"/>
        <source>Wallet: &lt;strong&gt;%1&lt;/strong&gt;</source>
        <translation>Kontostand: &lt;strong&gt;%1&lt;/strong&gt;</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="449"/>
        <source>IGB link was copied to the clipboard.</source>
        <translation>IGB Link wurde in die Zwischenablage kopiert.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="471"/>
        <source>HTTP link was copied to the clipboard.</source>
        <translation>HTTP Link wurde in die Zwischenablage kopiert.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="565"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="566"/>
        <source>Select character</source>
        <translation>Charakter wählen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="567"/>
        <source>&amp;Manage characters...</source>
        <translation>Charaktere &amp;verwalten...</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="569"/>
        <location filename="../MainWindow.cpp" line="571"/>
        <source>&amp;Preferences...</source>
        <translation>&amp;Einstellungen...</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="574"/>
        <source>Import EVE Mentat order history...</source>
        <translation>EVE Mentat Orderhistorie importieren...</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="576"/>
        <source>E&amp;xit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="578"/>
        <source>&amp;Tools</source>
        <translation>&amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="579"/>
        <source>Import conquerable stations</source>
        <translation>Importiere Stationen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="580"/>
        <source>Ma&amp;rgin tool...</source>
        <translation>Ma&amp;rgenrechner...</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="582"/>
        <source>Copy HTTP link</source>
        <translation>HTTP Link kopieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="583"/>
        <source>Copy IGB link</source>
        <translation>IGB Link kopieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="586"/>
        <source>Upload data to cloud...</source>
        <translation>Daten in die Cloud hochladen...</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="589"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="590"/>
        <source>Show/hide tabs</source>
        <translation>Register anzeigen/verbergen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="591"/>
        <source>Show/hide table columns</source>
        <translation>Spalten anzeigen/verbergen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="593"/>
        <source>Always on top</source>
        <translation>Immer im Vordergrund</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="625"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="626"/>
        <source>&amp;Online help...</source>
        <translation>&amp;Onlinehilfe...</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="627"/>
        <source>Check for &amp;updates</source>
        <translation>Auf &amp;Updates prüfen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="629"/>
        <source>&amp;About...</source>
        <translation>&amp;Über...</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="666"/>
        <source>Character</source>
        <translation>Charakter</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="689"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="706"/>
        <source>Assets</source>
        <translation>Gegenstände</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="728"/>
        <source>Character orders</source>
        <translation>Aufträge - Charakter</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="751"/>
        <source>Character journal</source>
        <translation>Logbuch - Charakter</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="763"/>
        <source>Character transactions</source>
        <translation>Transaktionen - Charakter</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="775"/>
        <source>Character contracts</source>
        <translation>Verträge - Charakter</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="790"/>
        <source>Corporation orders</source>
        <translation>Aufträge - Corporation</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="812"/>
        <source>Corporation journal</source>
        <translation>Logbuch - Corporation</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="824"/>
        <source>Corporation transactions</source>
        <translation>Transaktionen - Corporation</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="836"/>
        <source>Corporation contracts</source>
        <translation>Verträge - Corporation</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="847"/>
        <source>Item history</source>
        <translation>Verlauf Gegenstand</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="860"/>
        <source>Market browser</source>
        <translation>Marktbetrachter</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="872"/>
        <source>Item costs</source>
        <translation>Artikelkosten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="881"/>
        <source>LMeve</source>
        <translation>LMeve</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="902"/>
        <source>Market analysis</source>
        <translation>Marktanalyse</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="923"/>
        <source>Toggle character tabs</source>
        <translation>Charakter Registerkarten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="929"/>
        <source>Toggle corporation tabs</source>
        <translation>Körperschaftsteuer Registerkarten</translation>
    </message>
</context>
<context>
    <name>Evernus::MarginToolDialog</name>
    <message>
        <location filename="../MarginToolDialog.cpp" line="85"/>
        <source>Margin data</source>
        <translation>Margendaten</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="86"/>
        <source>Data source</source>
        <translation>Datenquelle</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="91"/>
        <source>Always on top</source>
        <translation>Immer im Vordergrund</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="98"/>
        <source>Quit application</source>
        <translation>Anwendung verlassen</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="109"/>
        <source>Margin tool</source>
        <translation>Margenrechner</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="449"/>
        <source>export market logs in game</source>
        <translation>Aus dem Spiel exportierte Markt Logs</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="458"/>
        <source>Sell:</source>
        <translation>Verkauf:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="459"/>
        <source>Buy:</source>
        <translation>Kauf:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="460"/>
        <source>Profit:</source>
        <translation>Profit:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="461"/>
        <source>Revenue:</source>
        <translation>Umsatz:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="462"/>
        <source>Cost of sales:</source>
        <translation>Unkosten:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="463"/>
        <source>Buyout:</source>
        <translation>Sofortkauf:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="477"/>
        <location filename="../MarginToolDialog.cpp" line="492"/>
        <source>Copy to clipboard</source>
        <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="513"/>
        <source>Buy orders:</source>
        <translation>Kaufaufträge:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="518"/>
        <source>Sell orders:</source>
        <translation>Verkaufsaufträge:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="523"/>
        <source>Buy volume/movement:</source>
        <translation>Volumen/Momentum Ankauf:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="528"/>
        <source>Sell volume/movement:</source>
        <translation>Volumen/Momentum Verkauf:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="541"/>
        <source>Margin:</source>
        <translation>Marge:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="547"/>
        <source>Markup:</source>
        <translation>Aufschlag:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="552"/>
        <source>Autocopy</source>
        <translation>Autokopie</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="562"/>
        <source>Nothing</source>
        <translation>Nichts</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="567"/>
        <source>Sell price</source>
        <translation>Verkaufspreis</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="572"/>
        <source>Buy price</source>
        <translation>Kaufpreis</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="594"/>
        <source>Broker fee:</source>
        <translation>Maklergebühr:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="599"/>
        <source>Sales tax:</source>
        <translation>Verkaufssteuer:</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="604"/>
        <source>Sample data</source>
        <translation>Beispieldaten</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="615"/>
        <source>If you experience problems with importing orders, adjust margin tool settings in the Preferences.</source>
        <translation>Wenn Problem beim Import von Aufträgen auftreten, ändere die Margenrechner-Parameter in Einstellungen.</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="627"/>
        <source>Preferred source</source>
        <translation>Bevorzugte Quelle</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="637"/>
        <source>Orders only</source>
        <translation>Nur AUfträge</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="641"/>
        <source>Prefer custom item costs (if available)</source>
        <translation>Bevorzuge benutzerdefinierte Kosten für Gegenstände (falls verfügbar)</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="645"/>
        <source>Custom station</source>
        <translation>Benutzerdefinierte Station</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="649"/>
        <source>Source station</source>
        <translation>Ursprungsstation</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="807"/>
        <location filename="../MarginToolDialog.cpp" line="813"/>
        <source>Margin tool error</source>
        <translation>Fehler Margenrechner</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="808"/>
        <source>Could not determine market log path. Please enter log path in settings.</source>
        <translation>Konnte das Verzeichnis für Markt Logs nicht bestimmen. Bitte das Verzeichnis in Einstellungen angeben.</translation>
    </message>
    <message>
        <location filename="../MarginToolDialog.cpp" line="814"/>
        <source>Could not start watching market log path. Make sure the path exists (eg. export some logs) and try again.</source>
        <translation>Konnte Überwachung der Markt Logs nicht starten. Stelle sicher das dieses Verzeichnis existiert (exportiere z.B. einige Logs) und versuche es erneut.</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketAnalysisWidget</name>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="162"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="89"/>
        <source>Import data</source>
        <translation>Importiere Daten</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="96"/>
        <source>Don&apos;t save imported orders (huge performance gain)</source>
        <translation>Importierte Aufträge nicht speichern (großer Performancezugewinn)</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="105"/>
        <source>Ignore types with existing orders</source>
        <translation>Ignoriere Artikel mit bestehenden Aufträgen</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="134"/>
        <source>Bogus order threshold:</source>
        <translation>Schwellenwert für unsinnige Aufträge:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="163"/>
        <source>Inter-Region</source>
        <translation>Interregional</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="562"/>
        <source>Station change</source>
        <translation>Stationswechsel</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="562"/>
        <source>Changing station requires data recalculation. Do you wish to do it now?</source>
        <translation>Ein Stationswechesel erfordert eine Datenneuberechnung. Möchtest Du dies nun tun?</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="607"/>
        <source>Region:</source>
        <translation>Region:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="627"/>
        <source>Limit to solar system:</source>
        <translation>Begrenze auf Sternensystem:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="640"/>
        <location filename="../MarketAnalysisWidget.cpp" line="885"/>
        <source>Volume:</source>
        <translation>Volumen:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="658"/>
        <location filename="../MarketAnalysisWidget.cpp" line="903"/>
        <source>Margin:</source>
        <translation>Marge:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="679"/>
        <source>Buy price:</source>
        <translation>Kaufpreis:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="695"/>
        <source>Sell price:</source>
        <translation>Verkaufspreis:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="711"/>
        <location filename="../MarketAnalysisWidget.cpp" line="921"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="715"/>
        <source>Double-click an item for additional information. &quot;Show in EVE&quot; is available via the right-click menu.</source>
        <translation>Zusatzinformationen zu einem Gegenstand mit Doppelklick. &quot;In EVE anzeigen&quot; ist via Rechtsklickmenü verfügbar.</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="720"/>
        <location filename="../MarketAnalysisWidget.cpp" line="935"/>
        <source>Calculating data...</source>
        <translation>Berechene Daten...</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="740"/>
        <source>Show details</source>
        <translation>Details anzeigen</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="745"/>
        <location filename="../MarketAnalysisWidget.cpp" line="959"/>
        <source>Show in EVE</source>
        <translation>In EVE anzeigen</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="750"/>
        <location filename="../MarketAnalysisWidget.cpp" line="964"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="558"/>
        <location filename="../MarketAnalysisWidget.cpp" line="787"/>
        <source>- any station -</source>
        <translation>- beliebige Station -</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="790"/>
        <source>Source:</source>
        <translation>Quelle:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="800"/>
        <source>Destination:</source>
        <translation>Ziel:</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="859"/>
        <source>- multiple -</source>
        <translation>- mehrere -</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="925"/>
        <source>&quot;Show in EVE&quot; is available via the right-click menu.</source>
        <translation>&quot;In EVE anzeigen&quot; ist via Rechtsklickmenü verfügbar.</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="118"/>
        <source>Discard bogus orders (causes recalculation)</source>
        <translation>Unsinnsaufträge verwerfen (startet Neuberechnung)</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="869"/>
        <source>- none -</source>
        <translation>- keine -</translation>
    </message>
    <message>
        <source>Copy &amp;rows</source>
        <translation type="obsolete">&amp;Zeilen kopieren</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="197"/>
        <source>Importing data for analysis...</source>
        <translation>Importiere Daten zur Analyse...</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="199"/>
        <source>Making %1 CREST order requests...</source>
        <translation>Erstelle %1 CREST Auftragsanfragen...</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="200"/>
        <source>Making %1 CREST history requests...</source>
        <translation>Erstelle %1 CREST Historieanfragen...</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="371"/>
        <source>%1 in %2</source>
        <translation>%1 in %2</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="443"/>
        <source>Waiting for %1 order server replies...</source>
        <translation>Warte auf %1 Serverantworten für Aufträge...</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="472"/>
        <source>Saving %1 imported orders...</source>
        <translation>Speichere %1 importierte Aufträge...</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="498"/>
        <source>Waiting for %1 history server replies...</source>
        <translation>Warte auf %1 Serverantworten für Historie...</translation>
    </message>
    <message>
        <location filename="../MarketAnalysisWidget.cpp" line="584"/>
        <location filename="../MarketAnalysisWidget.cpp" line="847"/>
        <location filename="../MarketAnalysisWidget.cpp" line="872"/>
        <location filename="../MarketAnalysisWidget.cpp" line="877"/>
        <source>- all -</source>
        <translation>- alle -</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketBrowserWidget</name>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="81"/>
        <source>Import prices from Web</source>
        <translation>Preisimport aus dem Web</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="86"/>
        <source>Import prices from logs</source>
        <translation>Preisimport aus Logs</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="91"/>
        <location filename="../MarketBrowserWidget.cpp" line="257"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="113"/>
        <source>Clean all orders</source>
        <translation>Alle Aufträge löschen</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="114"/>
        <source>Clean for selected type</source>
        <translation>Für ausgewählten Gegenstand löschen</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="116"/>
        <source>Cleanup  </source>
        <translation>Lösche</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="126"/>
        <source>Navigator</source>
        <translation>Navigator</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="134"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="140"/>
        <source>Forward</source>
        <translation>Vor</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="149"/>
        <source>Bookmarks</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="154"/>
        <source>Grouping</source>
        <translation>Gruppierung</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="159"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="167"/>
        <source>Station</source>
        <translation>Station</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="174"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="181"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="191"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="197"/>
        <source>My orders</source>
        <translation>Meine Aufträge</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="205"/>
        <source>Favorite</source>
        <translation>Favorit</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="208"/>
        <source>Add...</source>
        <translation>Hinzufügen...</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="213"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="224"/>
        <source>Regions [&lt;a href=&apos;#&apos;&gt;all&lt;/a&gt;]</source>
        <translation>Regionen [&lt;a href=&apos;#&apos;&gt;alle&lt;/a&gt;]</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="234"/>
        <source>Solar systems [&lt;a href=&apos;#&apos;&gt;all&lt;/a&gt;]</source>
        <translation>Sternensysteme [&lt;a href=&apos;#&apos;&gt;alle&lt;/a&gt;]</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="244"/>
        <source>Stations [&lt;a href=&apos;#&apos;&gt;all&lt;/a&gt;]</source>
        <translation>Stationen [&lt;a href=&apos;#&apos;&gt;alle&lt;/a&gt;]</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="264"/>
        <source>Min. price:</source>
        <translation>Min. Preis:</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="268"/>
        <location filename="../MarketBrowserWidget.cpp" line="276"/>
        <location filename="../MarketBrowserWidget.cpp" line="284"/>
        <location filename="../MarketBrowserWidget.cpp" line="291"/>
        <source>any</source>
        <translation>jeder</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="272"/>
        <source>Max. price:</source>
        <translation>Max. Preis:</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="280"/>
        <source>Min. volume:</source>
        <translation>Min. Volumen:</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="287"/>
        <source>Max. volume:</source>
        <translation>Max. Volumen:</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="294"/>
        <source>Security status:</source>
        <translation>Sicherheitsstatus:</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="299"/>
        <source>-1.0 - 0.0</source>
        <translation>-1.0-0.0</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="304"/>
        <source>0.1 - 0.4</source>
        <translation>0.1-0.4</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="309"/>
        <source>0.5 - 1.0</source>
        <translation>0.5-1.0</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="316"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="320"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="327"/>
        <location filename="../MarketBrowserWidget.cpp" line="946"/>
        <source>select an item</source>
        <translation>Wähle einen Gegenstand</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="331"/>
        <source>Sell orders</source>
        <translation>Verkaufsaufträge</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="340"/>
        <location filename="../MarketBrowserWidget.cpp" line="357"/>
        <source>Set as deviation reference</source>
        <translation>Als Referenz für Abweichung benutzen</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="348"/>
        <source>Buy orders</source>
        <translation>Kaufaufträge</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="453"/>
        <location filename="../MarketBrowserWidget.cpp" line="490"/>
        <location filename="../MarketBrowserWidget.cpp" line="722"/>
        <source>(all)</source>
        <translation>(alle)</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="595"/>
        <source>Add to favorites</source>
        <translation>Zu Favoriten hinzufügen</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="750"/>
        <source>Add bookmark</source>
        <translation>Lesezeichen hinzufügen</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="751"/>
        <source>Remove bookmark...</source>
        <translation>Lesezeichen entfernen...</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="779"/>
        <source>type in wildcard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="803"/>
        <source>Deviation [median]  </source>
        <translation>Abweichung [Durchschnitt]</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="805"/>
        <source>Deviation [best price]  </source>
        <translation>Abweichung [Bester Preis]</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="807"/>
        <source>Deviation [custom cost]  </source>
        <translation>Abweichung [Benutzerdefinierte Kosten]</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="809"/>
        <source>Deviation [fixed]  </source>
        <translation>Abweichung [Fest]</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="811"/>
        <source>Deviation  </source>
        <translation>Abweichung</translation>
    </message>
    <message>
        <location filename="../MarketBrowserWidget.cpp" line="944"/>
        <source>%1 (%2m³)</source>
        <translation>%1 (%2m³)</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketLogExternalOrderImporterThread</name>
    <message>
        <location filename="../MarketLogExternalOrderImporterThread.cpp" line="34"/>
        <source>Could not determine market log path!</source>
        <translation>Verzeichnis mit Mark Logs nicht gefunden!</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderArchiveModel</name>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="118"/>
        <source>Buy</source>
        <translation>Kauf</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="118"/>
        <source>Sell</source>
        <translation>Verkauf</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="126"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="127"/>
        <source>Closed</source>
        <translation>Geschlossen</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="128"/>
        <source>Fulfilled</source>
        <translation>Ausgeführt</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="129"/>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="130"/>
        <source>Pending</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="131"/>
        <source>Character Deleted</source>
        <translation>Character gelöscht</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="137"/>
        <source>Expired</source>
        <translation>Abgelaufen</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="222"/>
        <source>Completed</source>
        <translation>Abgeschlossen</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="224"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="226"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="228"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="230"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="232"/>
        <source>Custom cost</source>
        <translation>Benutzerdefinierte Kosten</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="234"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="236"/>
        <source>Volume</source>
        <translation>Menge</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="238"/>
        <source>Profit</source>
        <translation>Profit</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="240"/>
        <source>Station</source>
        <translation>Station</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="242"/>
        <source>Notes</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location filename="../MarketOrderArchiveModel.cpp" line="244"/>
        <source>Owner</source>
        <translation>Besitzer</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderBuyModel</name>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="84"/>
        <source>No price data -&gt; Please import prices from Orders/Assets tab or by using Margin tool.</source>
        <translation>Keine Preise -&gt; Bitte Preise im Register Aufträge/Gegenstände oder mit dem Margenrechner importieren.</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="90"/>
        <source>You have been overbid. Current price is %1 (%2 different from yours).
Click the icon for details.</source>
        <translation>Du wurdest überboten. Aktueller Preis: %1 (%2 Differenz zu deinem).
Mehr Details durch Klick auf das Icon.</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="99"/>
        <source>Price data is too old (valid on %1).
Please import prices from Orders/Assets tab or by using Margin tool.</source>
        <translation>Preise sind zu alt (Daten vom %1).
Bitte Preise im Register Aufträge/Gegenstände oder mit dem Margenrechner importieren.</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="103"/>
        <source>Your price was best on %1</source>
        <translation>Dein Preis war der Beste am %1</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="224"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="225"/>
        <source>Closed</source>
        <translation>Geschlossen</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="226"/>
        <source>Fulfilled</source>
        <translation>Ausgeführt</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="227"/>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="228"/>
        <source>Pending</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="229"/>
        <source>Character Deleted</source>
        <translation>Character gelöscht</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="235"/>
        <source>Expired</source>
        <translation>Abgelaufen</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="247"/>
        <source>No price data</source>
        <translation>Keine Preisdaten</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="252"/>
        <source>Data too old</source>
        <translation>Daten zu alt</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="280"/>
        <location filename="../MarketOrderBuyModel.cpp" line="450"/>
        <source>Station</source>
        <translation>Station</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="282"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="284"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="286"/>
        <source>%1 jumps</source>
        <translation>%1 Sprünge</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="295"/>
        <location filename="../MarketOrderBuyModel.cpp" line="299"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message numerus="yes">
        <location filename="../MarketOrderBuyModel.cpp" line="302"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n Tag</numerusform>
            <numerusform>%n Tage</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="302"/>
        <source>today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="416"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="418"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="420"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="422"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="424"/>
        <source>Price status</source>
        <translation>Preisstatus</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="426"/>
        <source>Price difference</source>
        <translation>Preisdifferenz</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="428"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="430"/>
        <source>Total</source>
        <translation>Gesamt</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="432"/>
        <source>Delta</source>
        <translation>Delta</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="434"/>
        <source>Order margin</source>
        <translation>Auftragsmarge</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="436"/>
        <source>Best margin</source>
        <translation>Beste Marge</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="438"/>
        <source>Range</source>
        <translation>Reichweite</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="440"/>
        <source>Min. quantity</source>
        <translation>Mindestmenge</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="442"/>
        <source>ETA</source>
        <translatorcomment>#NOTE# There isn&apos;t a fitting German abbreviation. If sapce allows, &quot;Geplante Ankunft&quot; is fine, otherwise stay with ETA</translatorcomment>
        <translation>ETA</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="444"/>
        <source>Time left</source>
        <translation>Restlaufzeit</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="446"/>
        <source>Order age</source>
        <translation>Auftragsalter</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="448"/>
        <source>First issued</source>
        <translation>Erstmals erstellt</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="452"/>
        <source>Notes</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location filename="../MarketOrderBuyModel.cpp" line="454"/>
        <source>Owner</source>
        <translation>Besitzer</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderFilterWidget</name>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="69"/>
        <source>Script</source>
        <translation>Skript</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="95"/>
        <source>type in script and press Enter</source>
        <translation>Skript eingeben und ENTER drücken</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="105"/>
        <source>Ch</source>
        <translation>Ch</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="107"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="109"/>
        <source>F</source>
        <translation>F</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="111"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="113"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="115"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="117"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="119"/>
        <source>Status filter</source>
        <translation>Statusfilter</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="119"/>
        <source>Status filter [%1]  </source>
        <translation>Statusfilter [%1]</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="126"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="128"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="130"/>
        <source>O</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="132"/>
        <source>Price status filter</source>
        <translation>Preisstatusfilter</translation>
    </message>
    <message>
        <location filename="../MarketOrderFilterWidget.cpp" line="132"/>
        <source>Price status filter [%1]  </source>
        <translation>Preisstatusfilter [%1]</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderInfoWidget</name>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="46"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="51"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="61"/>
        <source>Copy new price on open</source>
        <translation>Neuen Preis beim Öffnen kopieren</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="73"/>
        <source>&lt;span style=&apos;color: blue&apos;&gt;Your price:&lt;/span&gt;</source>
        <translation>&lt;span style=&apos;color: blue&apos;&gt;Dein Preis:&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="74"/>
        <source>&lt;span style=&apos;color: blue&apos;&gt;%1&lt;/span&gt;</source>
        <translation>&lt;span style=&apos;color: blue&apos;&gt;%1&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="76"/>
        <location filename="../MarketOrderInfoWidget.cpp" line="82"/>
        <source>Valid on:</source>
        <translation>Gültig am:</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="79"/>
        <source>&lt;span style=&apos;color: red&apos;&gt;Market price:&lt;/span&gt;</source>
        <translation>&lt;span style=&apos;color: red&apos;&gt;Marktpreis:&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="80"/>
        <source>&lt;span style=&apos;color: red&apos;&gt;%1&lt;/span&gt;</source>
        <translation>&lt;span style=&apos;color: red&apos;&gt;%1&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="85"/>
        <source>Difference:</source>
        <translation>Differenz:</translation>
    </message>
    <message>
        <location filename="../MarketOrderInfoWidget.cpp" line="87"/>
        <source>New price:</source>
        <translation>Neuer Preis:</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderPriceStatusesWidget</name>
    <message>
        <location filename="../MarketOrderPriceStatusesWidget.cpp" line="37"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../MarketOrderPriceStatusesWidget.cpp" line="38"/>
        <source>No data</source>
        <translation>Keine Daten (N)</translation>
    </message>
    <message>
        <location filename="../MarketOrderPriceStatusesWidget.cpp" line="39"/>
        <source>Data too old</source>
        <translation>Daten zu alt (O)</translation>
    </message>
    <message>
        <location filename="../MarketOrderPriceStatusesWidget.cpp" line="41"/>
        <source>Check all</source>
        <translation>Alle auswählen</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderSellModel</name>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="89"/>
        <source>No price data -&gt; Please import prices from Orders/Assets tab or by using Margin tool.</source>
        <translation>Keine Preise -&gt; Bitte Preise im Register Aufträge/Gegenstände oder mit dem Margenrechner importieren.</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="95"/>
        <source>You have been undercut. Current price is %1 (%2 different from yours).
Click the icon for details.</source>
        <translation>Du wurdest unterboten. Aktueller Preis: %1 (%2 Differenz zu deinem).
Mehr Details durch Klick auf das Icon.</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="104"/>
        <source>Price data is too old (valid on %1).
Please import prices from Orders/Assets tab or by using Margin tool.</source>
        <translation>Preise sind zu alt (Daten vom %1).
Bitte Preise im Register Aufträge/Gegenstände oder mit dem Margenrechner importieren.</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="108"/>
        <source>Your price was best on %1</source>
        <translation>Dein Preis war der Beste am %1</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="265"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="266"/>
        <source>Closed</source>
        <translation>Geschlossen</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="267"/>
        <source>Fulfilled</source>
        <translation>Ausgeführt</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="268"/>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="269"/>
        <source>Pending</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="270"/>
        <source>Character Deleted</source>
        <translation>Character gelöscht</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="276"/>
        <source>Expired</source>
        <translation>Abgelaufen</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="295"/>
        <source>No price data</source>
        <translation>Keine Preisdaten</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="300"/>
        <source>Data too old</source>
        <translation>Daten zu alt</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="367"/>
        <location filename="../MarketOrderSellModel.cpp" line="371"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message numerus="yes">
        <location filename="../MarketOrderSellModel.cpp" line="374"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n Tag</numerusform>
            <numerusform>%n Tage</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="374"/>
        <source>today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="496"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="498"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="500"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="502"/>
        <source>Custom cost</source>
        <translation>Benutzerdefinierte Kosten</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="504"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="506"/>
        <source>Price status</source>
        <translation>Preisstatus</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="508"/>
        <source>Price difference</source>
        <translation>Preisdifferenz</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="510"/>
        <source>Price difference, %1</source>
        <translation>Preisdifferenz, %1</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="512"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="514"/>
        <source>Total</source>
        <translation>Gesamt</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="516"/>
        <source>Delta</source>
        <translation>Delta</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="518"/>
        <source>Margin</source>
        <translation>Marge</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="520"/>
        <source>Best margin</source>
        <translation>Beste Marge</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="522"/>
        <source>Profit</source>
        <translation>Profit</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="524"/>
        <source>Total profit</source>
        <translation>Profit gesamt</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="526"/>
        <source>Profit per item</source>
        <translation>Profit pro Artikel</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="528"/>
        <source>ETA</source>
        <translation>ETA</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="530"/>
        <source>Time left</source>
        <translation>Restlaufzeit</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="532"/>
        <source>Order age</source>
        <translation>Auftragsalter</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="534"/>
        <source>First issued</source>
        <translation>Erstmals erstellt</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="536"/>
        <source>Station</source>
        <translation>Station</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="538"/>
        <source>Notes</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location filename="../MarketOrderSellModel.cpp" line="540"/>
        <source>Owner</source>
        <translation>Besitzer</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderStatesWidget</name>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="41"/>
        <source>Changed</source>
        <translation>Geändert (Ch)</translation>
    </message>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="45"/>
        <source>Active</source>
        <translation>Aktiv (A)</translation>
    </message>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="46"/>
        <source>Fulfilled</source>
        <translation>Ausgeführt (F)</translation>
    </message>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="47"/>
        <source>Cancelled</source>
        <translation>Abgebrochen (C)</translation>
    </message>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="48"/>
        <source>Pending</source>
        <translation>Ausstehend (P)</translation>
    </message>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="49"/>
        <source>Deleted</source>
        <translation>Gelöscht (D)</translation>
    </message>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="50"/>
        <source>Expired</source>
        <translation>Abgelaufen (E)</translation>
    </message>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="52"/>
        <source>Check all</source>
        <translation>Alle markieren</translation>
    </message>
    <message>
        <location filename="../MarketOrderStatesWidget.cpp" line="56"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderView</name>
    <message>
        <location filename="../MarketOrderView.cpp" line="63"/>
        <source>Delete order</source>
        <translation>Auftrag löschen</translation>
    </message>
    <message>
        <location filename="../MarketOrderView.cpp" line="68"/>
        <source>Show in market browser</source>
        <translation>Im Marktbetrachter anzeigen</translation>
    </message>
    <message>
        <location filename="../MarketOrderView.cpp" line="73"/>
        <source>Show in EVE</source>
        <translation>In EVE anzeigen</translation>
    </message>
    <message>
        <location filename="../MarketOrderView.cpp" line="78"/>
        <source>Change notes</source>
        <translation>Notiz bearbeiten</translation>
    </message>
    <message>
        <location filename="../MarketOrderView.cpp" line="89"/>
        <source>Lookup item on eve-marketdata.com</source>
        <translation>Artikel auf eve-marketdata.com nachschauen</translation>
    </message>
    <message>
        <location filename="../MarketOrderView.cpp" line="92"/>
        <source>Lookup item on eve-central.com</source>
        <translation>Artikel auf eve-central.com nachschauen</translation>
    </message>
    <message>
        <location filename="../MarketOrderView.cpp" line="169"/>
        <source>Notes</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location filename="../MarketOrderView.cpp" line="169"/>
        <source>Notes:</source>
        <translation>Notiz:</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderViewWithTransactions</name>
    <message>
        <location filename="../MarketOrderViewWithTransactions.cpp" line="83"/>
        <source>Market orders</source>
        <translation>Marktaufträge</translation>
    </message>
    <message>
        <location filename="../MarketOrderViewWithTransactions.cpp" line="90"/>
        <source>Transactions</source>
        <translation>Transaktionen</translation>
    </message>
</context>
<context>
    <name>Evernus::MarketOrderWidget</name>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="74"/>
        <source>File import</source>
        <translation>Dateiimport</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="84"/>
        <source>Import prices from Web</source>
        <translation>Preisimport aus dem Web</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="85"/>
        <source>Import prices from logs</source>
        <translation>Preisimport aus Logs</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="87"/>
        <source>Import prices  </source>
        <translation>Preise importieren</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="92"/>
        <source>Open margin tool</source>
        <translation>Margenrechner öffnen</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="100"/>
        <source>Group by:</source>
        <translation>Gruppiere nach:</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="104"/>
        <source>- none -</source>
        <translation>- keine -</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="105"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="106"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="107"/>
        <source>Station</source>
        <translation>Station</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="128"/>
        <source>Sell</source>
        <translation>Verkauf</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="152"/>
        <source>Buy</source>
        <translation>Kauf</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="164"/>
        <source>Sell &amp;&amp; Buy</source>
        <translation>Verkauf &amp;&amp;Kauf</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="168"/>
        <source>Sell orders</source>
        <translation>Verkaufsaufträge</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="187"/>
        <source>Buy orders</source>
        <translation>Kaufaufträge</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="207"/>
        <source>History</source>
        <translation>Historie</translation>
    </message>
    <message>
        <location filename="../MarketOrderWidget.cpp" line="305"/>
        <source>Script error</source>
        <translation>Skriptfehler</translation>
    </message>
</context>
<context>
    <name>Evernus::MenuBarWidget</name>
    <message>
        <location filename="../MenuBarWidget.cpp" line="34"/>
        <source>Import all</source>
        <translation>Alle importieren</translation>
    </message>
</context>
<context>
    <name>Evernus::NetworkPreferencesWidget</name>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="55"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="60"/>
        <source>No proxy</source>
        <translation>Kein Proxy</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="63"/>
        <source>Custom proxy</source>
        <translation>Benutzerdefinierter Proxy</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="73"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="74"/>
        <source>SOCKS5</source>
        <translation>SOCKS5</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="75"/>
        <source>HTTP</source>
        <translation>HTTP</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="78"/>
        <source>Host:</source>
        <translation>Host:</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="81"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="87"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="90"/>
        <source>Password:</source>
        <translation>Paßwort:</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="93"/>
        <source>Warning: password store uses weak encryption - do not use sensitive passwords.</source>
        <translation>Warnung: Paßwörter werden mit einer schwacher Verschlüsselung gespeichert - benutze keine sensiblen Paßwörter.</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="97"/>
        <source>API provider</source>
        <translation>API Anbieter</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="102"/>
        <source>Use default provider</source>
        <translation>Standardanbieter benutzen</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="107"/>
        <source>Use custom provider</source>
        <translation>Benutzerdefinierten Anbieter benutzen</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="114"/>
        <source>CREST</source>
        <translation>CREST</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="120"/>
        <source>Max. threads:</source>
        <translation>Max. Threads:</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="123"/>
        <source>This value affects the speed of importing data via CREST. Higher number gives more speed, but too high value can cause the speed to drop and/or create import errors.</source>
        <translation>Dieser Wert beeinflußt die Geschwindigkeit des Datenimports via CREST. Ein höherer Wert erhöht die Geschwindigkeit, ist er jedoch zu hoch, kann die Geschwindigkeit sogar sinken oder Fehler verursachen.</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="135"/>
        <source>Max. reply time:</source>
        <translation>Max. Antwortzeit:</translation>
    </message>
    <message>
        <location filename="../NetworkPreferencesWidget.cpp" line="141"/>
        <source>Ignore certificate errors</source>
        <translation>Ignorieren Zertifikatfehler</translation>
    </message>
</context>
<context>
    <name>Evernus::OrderPreferencesWidget</name>
    <message>
        <location filename="../OrderPreferencesWidget.cpp" line="41"/>
        <source>Max. market order age:</source>
        <translation>Max. Alter Marktaufträge:</translation>
    </message>
    <message>
        <location filename="../OrderPreferencesWidget.cpp" line="44"/>
        <location filename="../OrderPreferencesWidget.cpp" line="57"/>
        <source> days</source>
        <translation> Tage</translation>
    </message>
    <message>
        <location filename="../OrderPreferencesWidget.cpp" line="49"/>
        <source>Delete old fulfilled orders</source>
        <translation>Lösche alte ausgeführte Aufträge</translation>
    </message>
    <message>
        <location filename="../OrderPreferencesWidget.cpp" line="54"/>
        <source>Delete older than:</source>
        <translation>Lösche Aufträge älter als:</translation>
    </message>
</context>
<context>
    <name>Evernus::PathPreferencesWidget</name>
    <message>
        <location filename="../PathPreferencesWidget.cpp" line="39"/>
        <source>Market logs path</source>
        <translation>Verzeichnis Markt Logs</translation>
    </message>
    <message>
        <source>You can specify custom market logs path or leave empty to use the default one. Custom path is required on *nix systems.</source>
        <translatorcomment>#TYPO# There&apos;s an &quot;a&quot; and &quot;it&quot; missing: You can specify *a* custom ... or leave *it* empty ...</translatorcomment>
        <translation type="vanished">Du kannst ein benutzerdefiniertes Verzeichnis für die Markt Logs angeben (notwendig für *nix Systeme) oder das Standardverzeichnis verwenden.</translation>
    </message>
    <message>
        <location filename="../PathPreferencesWidget.cpp" line="44"/>
        <source>You can specify custom market logs path or leave it empty to use the default one. Custom path is required on *nix systems.</source>
        <translation>Du kannst ein benutzerdefiniertes Verzeichnis für die Markt Logs angeben (notwendig für *nix Systeme) oder das Standardverzeichnis verwenden.</translation>
    </message>
    <message>
        <location filename="../PathPreferencesWidget.cpp" line="56"/>
        <source>Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../PathPreferencesWidget.cpp" line="60"/>
        <source>Delete parsed logs</source>
        <translation>Ausgelesene Logs löschen</translation>
    </message>
    <message>
        <location filename="../PathPreferencesWidget.cpp" line="66"/>
        <source>Character log file name wildcard:</source>
        <translation>Charakter Logdatei Wildcard:</translation>
    </message>
    <message>
        <location filename="../PathPreferencesWidget.cpp" line="70"/>
        <source>Corporation log file name wildcard:</source>
        <translation>Corporationr Logdatei Wildcard:</translation>
    </message>
</context>
<context>
    <name>Evernus::PreferencesDialog</name>
    <message>
        <location filename="../PreferencesDialog.cpp" line="64"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="65"/>
        <source>Paths</source>
        <translation>Pfade</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="66"/>
        <source>Sounds</source>
        <translation>Klänge</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="67"/>
        <source>Prices</source>
        <translation>Preise</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="68"/>
        <source>Orders</source>
        <translation>Aufträge</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="69"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="70"/>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="71"/>
        <source>Synchronization</source>
        <translation>Synchronisation</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="72"/>
        <source>In-Game Browser</source>
        <translation>In-Game Browser</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="73"/>
        <source>Web Service</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="74"/>
        <source>Wallet</source>
        <translation>Konto</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="75"/>
        <source>LMeve</source>
        <translation>LMeve</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="92"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="96"/>
        <source>Character</source>
        <translation>Charakter</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="97"/>
        <source>Assets</source>
        <translation>Gegenstände</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="98"/>
        <source>Contracts</source>
        <translation>Verträge</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="99"/>
        <source>Corporation</source>
        <translation>Corporation</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="100"/>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="125"/>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>Evernus::PricePreferencesWidget</name>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="42"/>
        <source>Margins</source>
        <translation>Margen</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="50"/>
        <source>Minimum:</source>
        <translation>Minimum:</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="57"/>
        <source>Preferred:</source>
        <translation>Bevorzugt:</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="64"/>
        <source>Import log wait timer:</source>
        <translation>Wartezeit Log Import:</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="73"/>
        <source>Use alternative margin import method*</source>
        <translation>Benutze alternative Margenimportmethode*</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="77"/>
        <source>* Gives faster results, but can sometimes be incorrect. If the price fluctuates after a few imports, turn it off.</source>
        <translation>* Erzielt schneller Ergebnisse, kann aber inakurat sein. Wenn die Preise nach ein paar Importen fluktuieren, deaktiviere es.</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="82"/>
        <source>Costs</source>
        <translation>Kosten</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="87"/>
        <source>Auto add custom item costs on fulfilled buy order*</source>
        <translation>Automatisch benutzerdefinierte Kosten auf erfüllte Kaufaufträge aufschlagen*</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="91"/>
        <source>Share costs between characters</source>
        <translation>Kosten auf Charaktere aufteilen</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="95"/>
        <source>* Also turns on importing wallet transactions.</source>
        <translation>* Aktiviert auch den Import von Konto-Transaktionen.</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="105"/>
        <source>Price delta:</source>
        <translation>Preisdelta:</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="113"/>
        <source>Max. price age:</source>
        <translation>Max. Alter Preise:</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="122"/>
        <source>Plot number format:</source>
        <translation>Zahlenformat Diagram:</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="123"/>
        <source>beautified scientific</source>
        <translation>Leserliche wissenschaftliche Darstellung</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="124"/>
        <source>scientific</source>
        <translation>Wissenschaftliche Darstellung</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="125"/>
        <source>fixed</source>
        <translation>Fest</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="127"/>
        <source>Combine character and corporation journal in statistics</source>
        <translation>In Statistiken Charakter und Corporation Logbuch zusammenfassen</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="132"/>
        <source>Refresh prices after order import</source>
        <translation>Preise nach Auftragsimport aktualisieren</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="136"/>
        <source>Auto-copy non-overbid prices with price helper</source>
        <translation>Automatisch nicht überbotene Preise mit dem Preishelfer kopieren</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="140"/>
        <source>Limit sell price copy to item cost</source>
        <translation>Kopie des Verkauspreis auf Artikelkosten beschränken</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="144"/>
        <source>Fast Price Copy</source>
        <translation>Schnellkopie Preis</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="152"/>
        <source>Enabled</source>
        <translation>Aktivieren</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="156"/>
        <source>Shortcut:</source>
        <translation>Schnellzugriff:</translation>
    </message>
    <message>
        <source>Shourtcut:</source>
        <translatorcomment>#TYPO#</translatorcomment>
        <translation type="vanished">Schnellzugriff:</translation>
    </message>
    <message>
        <location filename="../PricePreferencesWidget.cpp" line="162"/>
        <source>Fast Price Copy allows you to update your orders in a very fast manner. Simply assign a keyboard shortcut, select an order in any market order view and press the shortcut to copy the updated price and automatically jump to the next order on the list. You can do this even when Evernus doesn&apos;t have the input focus - the keyboard shortcut works system-wide. You can use this to update your prices without ever leaving Eve client.</source>
        <translation>&quot;Schnellkopie Preis&quot; erlaubt es dir deine Aufträge sehr schnell zu aktualisieren. Ordne der Funktion ein Tastaturkürzel zu, wähle einen Auftrag in einer beliebigen Marktansicht aus, drück das Tastenkürzel, um den aktualisierten Preis zu kopieren und automatisch zum nächsten Auftrag in der Liste zu springen. Das funktioniert, selbst wenn Evernus nicht im Vordergrund ist, da das Tastenkürzel systemweit gilt. So kannst du deine Aufträge aktualisieren ohne jemals EVE zu verlassen.</translation>
    </message>
</context>
<context>
    <name>Evernus::RegionTypeSelectDialog</name>
    <message>
        <location filename="../RegionTypeSelectDialog.cpp" line="48"/>
        <source>Regions</source>
        <translation>Regionen</translation>
    </message>
    <message>
        <location filename="../RegionTypeSelectDialog.cpp" line="73"/>
        <source>Select all</source>
        <translation>Alle auswählen</translation>
    </message>
    <message>
        <location filename="../RegionTypeSelectDialog.cpp" line="77"/>
        <source>Deselect all</source>
        <translation>Alle abwählen</translation>
    </message>
    <message>
        <location filename="../RegionTypeSelectDialog.cpp" line="81"/>
        <source>Select without wormholes</source>
        <translation>Auswahl ohne Wurmlöcher</translation>
    </message>
    <message>
        <location filename="../RegionTypeSelectDialog.cpp" line="94"/>
        <source>Types</source>
        <translation>Typen</translation>
    </message>
    <message>
        <location filename="../RegionTypeSelectDialog.cpp" line="120"/>
        <source>Select regions and types</source>
        <translation>Regionen und Typen auswählen</translation>
    </message>
    <message>
        <location filename="../RegionTypeSelectDialog.cpp" line="146"/>
        <source>Order import</source>
        <translation>Auftragsimport</translation>
    </message>
    <message>
        <location filename="../RegionTypeSelectDialog.cpp" line="146"/>
        <source>Please select at least one region and type.</source>
        <translation>Bitte mindestens eine Region und einen Typ auswählen.</translation>
    </message>
</context>
<context>
    <name>Evernus::SellMarketOrdersInfoWidget</name>
    <message>
        <location filename="../SellMarketOrdersInfoWidget.cpp" line="36"/>
        <source>Total income:</source>
        <translation>Einkünfte gesamt:</translation>
    </message>
    <message>
        <location filename="../SellMarketOrdersInfoWidget.cpp" line="43"/>
        <source>Total cost:</source>
        <translation>Kosten gesamt:</translation>
    </message>
    <message>
        <location filename="../SellMarketOrdersInfoWidget.cpp" line="50"/>
        <source>Total margin:</source>
        <translation>Marge gesamt:</translation>
    </message>
</context>
<context>
    <name>Evernus::SoundPreferencesWidget</name>
    <message>
        <location filename="../SoundPreferencesWidget.cpp" line="38"/>
        <source>Fast Price Copy sound</source>
        <translation>Klang für Schnellkopie Preis</translation>
    </message>
</context>
<context>
    <name>Evernus::StationSelectDialog</name>
    <message>
        <location filename="../StationSelectDialog.cpp" line="30"/>
        <source>Any station</source>
        <translation>Beliebige Station</translation>
    </message>
    <message>
        <location filename="../StationSelectDialog.cpp" line="34"/>
        <source>Custom station</source>
        <translation>Benutzerdefinierte Station</translation>
    </message>
    <message>
        <location filename="../StationSelectDialog.cpp" line="47"/>
        <source>Select station</source>
        <translation>Station auswählen</translation>
    </message>
</context>
<context>
    <name>Evernus::StatisticsPreferencesWidget</name>
    <message>
        <location filename="../StatisticsPreferencesWidget.cpp" line="32"/>
        <source>Appearance</source>
        <translation>Aussehen</translation>
    </message>
    <message>
        <location filename="../StatisticsPreferencesWidget.cpp" line="43"/>
        <source>Asset value plot color:</source>
        <translation>Artikelwert Farbe:</translation>
    </message>
    <message>
        <location filename="../StatisticsPreferencesWidget.cpp" line="48"/>
        <source>Wallet balance plot color:</source>
        <translation>Kontostand Farbe:</translation>
    </message>
    <message>
        <location filename="../StatisticsPreferencesWidget.cpp" line="53"/>
        <source>Corp. wallet balance plot color:</source>
        <translation>Corp. Kontostand Farbe:</translation>
    </message>
    <message>
        <location filename="../StatisticsPreferencesWidget.cpp" line="58"/>
        <source>Buy order value plot color:</source>
        <translation>Wert Kaufauftrag Farbe:</translation>
    </message>
    <message>
        <location filename="../StatisticsPreferencesWidget.cpp" line="63"/>
        <source>Sell order value plot color:</source>
        <translation>Wert Verkaufsauftrag Farbe:</translation>
    </message>
    <message>
        <location filename="../StatisticsPreferencesWidget.cpp" line="68"/>
        <source>Total value plot color:</source>
        <translation>Wert gesamt Farbe:</translation>
    </message>
    <message>
        <location filename="../StatisticsPreferencesWidget.cpp" line="70"/>
        <source>Reset to default</source>
        <translation>Auf Werkseinstellungen zurück setzen</translation>
    </message>
</context>
<context>
    <name>Evernus::StatisticsWidget</name>
    <message>
        <location filename="../StatisticsWidget.cpp" line="94"/>
        <source>Basic</source>
        <translation>Einfach</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="95"/>
        <source>Advanced</source>
        <translation>Fortgeschritten</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="642"/>
        <source>Script error</source>
        <translation>Skriptfehler</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="648"/>
        <source>Save script</source>
        <translation>Skript speichern</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="648"/>
        <source>Enter script name:</source>
        <translation>Skriptnamen eingeben:</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="663"/>
        <source>Load script</source>
        <translation>Skript laden</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="663"/>
        <location filename="../StatisticsWidget.cpp" line="680"/>
        <source>Select script:</source>
        <translation>Skript auswählen:</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="680"/>
        <source>Delete script</source>
        <translation>Skript löschen</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="709"/>
        <source>Assets: %1
Wallet: %2
Corp. wallet: %3
Buy orders: %4
Sell orders: %5
Total: %6</source>
        <translation>Gegenstände: %1
Kontostand: %2
Corp. Kontostand: %3
Wert Kaufauftrag: %4
Wert Verkaufsauftrag: %5
Wert gesamt: %6</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="722"/>
        <source>Asset value</source>
        <translation>Artikelwert</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="725"/>
        <source>Wallet balance</source>
        <translation>Kontostand</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="728"/>
        <source>Corp. wallet balance</source>
        <translation>Corp. Kontostand</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="731"/>
        <source>Buy order value</source>
        <translation>Wert Kaufauftrag</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="734"/>
        <source>Sell order value</source>
        <translation>Wert Verkaufsauftrag</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="737"/>
        <source>Total value</source>
        <translation>Wert gesamt</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="741"/>
        <source>Incoming</source>
        <translation>Einnahmen</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="742"/>
        <source>Outgoing</source>
        <translation>Ausgaben</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="744"/>
        <source>Sell</source>
        <translation>Verkauf</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="745"/>
        <source>Buy</source>
        <translation>Kauf</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="785"/>
        <source>Combine statistics for all characters</source>
        <translation>Statistiken für alle Charaktere zusammenfassen</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="795"/>
        <source>Balance</source>
        <translation>Kontostand</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="805"/>
        <source>Wallet journal</source>
        <translation>Konto-Logbuch</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="820"/>
        <location filename="../StatisticsWidget.cpp" line="852"/>
        <source>Total income:</source>
        <translation>Einkünfte gesamt:</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="826"/>
        <location filename="../StatisticsWidget.cpp" line="858"/>
        <source>Total cost:</source>
        <translation>Kosten gesamt:</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="832"/>
        <location filename="../StatisticsWidget.cpp" line="864"/>
        <source>Balance:</source>
        <translation>Kontostand:</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="840"/>
        <source>Wallet transactions</source>
        <translation>Konto-Transaktionen</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="882"/>
        <source>This tab allows you to create custom reports aggregating historic market order data.</source>
        <translation>Diese Register ermöglicht die Erstellung benutzerdefinierter Reporte zusammengefaßter historischer Marktdaten.</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="890"/>
        <source>Simple aggregation</source>
        <translation>Einfache Zusammenfassung</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="900"/>
        <source>Group by:</source>
        <translation>Gruppiere nach:</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="904"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="905"/>
        <source>Location</source>
        <translation>Standort</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="907"/>
        <source>Order by:</source>
        <translation>Sortieren nach:</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="911"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="912"/>
        <source>Count</source>
        <translation>Anzahl</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="913"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="914"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="916"/>
        <source>Limit:</source>
        <translation>Limit:</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="922"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="924"/>
        <source>Include active</source>
        <translation>Aktive einschließen</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="927"/>
        <source>Include expired/cancelled</source>
        <translation>Abgelaufene/Abgebrochene einschließen</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="930"/>
        <location filename="../StatisticsWidget.cpp" line="961"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="937"/>
        <source>Script processing</source>
        <translation>Skriptverarbeitung</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="953"/>
        <source>see the online help to learn how to use script processing</source>
        <translation>Konsultiere die Onlinhilfe, um zu erfahren wie die Skriptverarbeitung verwendet werden kann</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="966"/>
        <source>Save script...</source>
        <translation>Skript speichern...</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="970"/>
        <source>Load script...</source>
        <translation>Skript laden...</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="974"/>
        <source>Delete script...</source>
        <translation>Skript löschen...</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="978"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="983"/>
        <source>For each</source>
        <translation>Für alle</translation>
    </message>
    <message>
        <location filename="../StatisticsWidget.cpp" line="987"/>
        <source>Aggregate</source>
        <translation>Zusammenfassen</translation>
    </message>
</context>
<context>
    <name>Evernus::StyledTreeView</name>
    <message>
        <location filename="../StyledTreeView.cpp" line="39"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="../StyledTreeView.cpp" line="45"/>
        <source>Copy &amp;rows</source>
        <translation>&amp;Zeilen kopieren</translation>
    </message>
    <message>
        <location filename="../StyledTreeView.cpp" line="49"/>
        <source>Copy raw &amp;data</source>
        <translation>Roh&amp;daten kopieren</translation>
    </message>
    <message>
        <location filename="../StyledTreeView.cpp" line="55"/>
        <source>Show/hide columns</source>
        <translation>Spalten anzeigen/verbergen</translation>
    </message>
</context>
<context>
    <name>Evernus::SyncDialog</name>
    <message>
        <location filename="../SyncDialog.cpp" line="74"/>
        <source>Synchronizing...</source>
        <translation>Synchronisiere...</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="78"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="97"/>
        <source>Proceed</source>
        <translation>Fortsetzen</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="101"/>
        <location filename="../SyncDialog.cpp" line="162"/>
        <location filename="../SyncDialog.cpp" line="191"/>
        <location filename="../SyncDialog.cpp" line="213"/>
        <location filename="../SyncDialog.cpp" line="266"/>
        <location filename="../SyncDialog.cpp" line="282"/>
        <location filename="../SyncDialog.cpp" line="304"/>
        <location filename="../SyncDialog.cpp" line="312"/>
        <source>Synchronization</source>
        <translation>Synchronisation</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="142"/>
        <source>Dropbox requires authenticating Evernus first. Please click on &lt;a href=&apos;%1&apos;&gt;this link&lt;/a&gt;, authorize Evernus and press &apos;Proceed&apos;.</source>
        <translation>Evernus muß zuerst von Dropbox authentifiziert werden. Bitte auf &lt;a href=&apos;%1&apos;&gt; diesen Link&lt;/a&gt; klicken, Evernus authentifizieren und &quot;Fortsetzen&quot; anklicken.</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="162"/>
        <source>Error: %1 (%2)</source>
        <translation>Fehler: %1 (%2)</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="192"/>
        <source>Your local database is newer than cloud one. Do you wish to replace your local copy?</source>
        <translation>Ihre lokale Datenbank ist neuer als eine Wolke. Möchten Sie Ihre lokale Kopie ersetzen möchten?</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="214"/>
        <source>Something modified cloud data since last synchronization. Do you wish to overwrite it?</source>
        <translation>Die Daten in der Cloud wurden seit der letzten Synchronisation geändert. Sollen die Daten überschrieben werden?</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="266"/>
        <source>Couldn&apos;t open file for writing! Synchronization failed.</source>
        <translation>Konnte die Datei nicht zum Schreiben öffnen! Synchronisation fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="282"/>
        <source>Couldn&apos;t write destination file! Synchronization failed.</source>
        <translation>Konnte in die Zieldatei nicht Schreiben! Synchronisation fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="304"/>
        <source>Couldn&apos;t open remote file! Synchronization failed.</source>
        <translation>Konnte die Datei in der Cloud nicht öffnen! Synchronisation fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../SyncDialog.cpp" line="312"/>
        <source>Couldn&apos;t open local file! Synchronization failed.</source>
        <translation>Konnte die lokale Datei nicht öffnen! Synchronisation fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>Evernus::SyncPreferencesWidget</name>
    <message>
        <location filename="../SyncPreferencesWidget.cpp" line="40"/>
        <source>Download on startup</source>
        <translation>Runterladen bei Programmstart</translation>
    </message>
    <message>
        <location filename="../SyncPreferencesWidget.cpp" line="44"/>
        <source>Upload on shutdown</source>
        <translation>Hochladen bei Programmende</translation>
    </message>
    <message>
        <location filename="../SyncPreferencesWidget.cpp" line="50"/>
        <source>Evernus was compiled without Dropbox app key and secret values. In order to enable Dropbox support, register a new app key with Dropbox and recompile Evernus with EVERNUS_DROPBOX_APP_KEY and EVERNUS_DROPBOX_APP_SECRET defined.</source>
        <translation>Evernus wurde ohne Dropbox App Key und Secret kompiliert. Um den Dropbox-Support zu aktivieren, registriere einen neuen App Key bei Dropbox und rekompiliere Evernus mit definierten EVERNUS_DROPBOX_APP_KEY und EVERNUS_DROPBOX_APP_SECRET.</translation>
    </message>
</context>
<context>
    <name>Evernus::TextFilterWidget</name>
    <message>
        <location filename="../TextFilterWidget.cpp" line="50"/>
        <source>type in wildcard and press Enter</source>
        <translation>Bezeichnung eingeben und ENTER drücken</translation>
    </message>
    <message>
        <location filename="../TextFilterWidget.cpp" line="91"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>Evernus::TradeableTypesTreeModel</name>
    <message>
        <location filename="../TradeableTypesTreeModel.cpp" line="213"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
</context>
<context>
    <name>Evernus::TypeAggregatedDetailsWidget</name>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="50"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="64"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="76"/>
        <source>Moving average days:</source>
        <translation>Tage gleitender Durchschnitt:</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="85"/>
        <source>MACD days:</source>
        <translation>Tage MACD:</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="102"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="106"/>
        <source>Add trend line</source>
        <translation>Trennlinie hinzufügen</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="112"/>
        <source>Show legend</source>
        <translation>Legende anzeigen</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="138"/>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="161"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="171"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="178"/>
        <source>SMA</source>
        <translation>SMA</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="182"/>
        <source>Bollinger upper band</source>
        <translation>Bollinger oberes Band</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="186"/>
        <source>Bollinger lower band</source>
        <translation>Bollinger unteres Band</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="198"/>
        <source>RSI (14 days)</source>
        <translation>RSI (14 Tage)</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="226"/>
        <source>RSI</source>
        <translation>RSI</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="235"/>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="256"/>
        <source>MACD</source>
        <translation>MACD</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="249"/>
        <source>MACD Divergence</source>
        <translation>MACD Divergenz</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedDetailsWidget.cpp" line="259"/>
        <source>MACD Signal</source>
        <translation>MACD Signal</translation>
    </message>
</context>
<context>
    <name>Evernus::TypeAggregatedMarketDataModel</name>
    <message>
        <location filename="../TypeAggregatedMarketDataModel.cpp" line="108"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedMarketDataModel.cpp" line="110"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedMarketDataModel.cpp" line="112"/>
        <source>5% volume buy price</source>
        <translation>5% Spanne Kaufpreis</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedMarketDataModel.cpp" line="114"/>
        <source>5% volume sell price</source>
        <translation>5% Spanne Verkaufspreis</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedMarketDataModel.cpp" line="116"/>
        <source>Difference</source>
        <translation>Differenz</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedMarketDataModel.cpp" line="118"/>
        <source>30-day avg. volume</source>
        <translation>30 Tage Durchschnittsvolumen</translation>
    </message>
    <message>
        <location filename="../TypeAggregatedMarketDataModel.cpp" line="120"/>
        <source>Margin</source>
        <translation>Marge</translation>
    </message>
</context>
<context>
    <name>Evernus::Updater</name>
    <message>
        <location filename="../Updater.cpp" line="296"/>
        <location filename="../Updater.cpp" line="327"/>
        <location filename="../Updater.cpp" line="334"/>
        <location filename="../Updater.cpp" line="346"/>
        <source>Update</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="296"/>
        <source>An error occurred during the update process.
Database backup was saved as %1. Please read online help how to deal with this situation.</source>
        <translation>Während des Updateprozess ist ein Fehler aufgetreten.
Ein Backup der Datenbank wurde als %1 gespeichert. Bitte schaue in der Onlinehilfe nach was in dieser Situation zu tun ist.</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="112"/>
        <location filename="../Updater.cpp" line="123"/>
        <location filename="../Updater.cpp" line="133"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="112"/>
        <source>Error contacting update server: %1</source>
        <translation>Fehler beim Verbinden mit dem Updateserver: %1</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="123"/>
        <source>Error parsing response from the update server: %1</source>
        <translation>Fehler beim Auswerten der Antwort des Updateservers: %1</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="133"/>
        <source>Missing update version information!</source>
        <translation>Fehlende Update-Versionsinformationen!</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="146"/>
        <source>No update found</source>
        <translation>Kein Update vorhanden</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="146"/>
        <source>Your current version is up-to-date.</source>
        <translation>Deine Version ist aktuell.</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="155"/>
        <location filename="../Updater.cpp" line="160"/>
        <location filename="../Updater.cpp" line="171"/>
        <source>Update found</source>
        <translation>Update gefunden</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="155"/>
        <source>A new version is available: %1
Do you wish to download it now?</source>
        <translation>Eine neue Version ist verfügbar: %1
Willst Du sie nun herunterladen?</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="160"/>
        <source>A new version is available: %1
Do you wish to launch the updater?</source>
        <translation>Eine neue Version ist verfügbar: %1
Wollen Sie den Updater starten möchten?</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="171"/>
        <source>Couldn&apos;t launch updater. Download manually?</source>
        <translation>Lässt sich nicht starten Updater. Manuell herunterladen?</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="280"/>
        <location filename="../Updater.cpp" line="286"/>
        <location filename="../Updater.cpp" line="292"/>
        <source>Error updating db version: %1</source>
        <translation>Fehler beim Aktualisieren der Datenbankversion: %1</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="327"/>
        <source>This update requires re-importing all data.
Please click on &quot;Import all&quot; after the update.</source>
        <translation>Diese Update erfordert den Reimport aller Daten. Bitte klicke nach dem Update auf &quot;Alle importieren&quot;.</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="334"/>
        <source>This update requires re-importing all item prices.</source>
        <translation>Diese Update erfordert den Reimport aller Artikelpreise.</translation>
    </message>
    <message>
        <location filename="../Updater.cpp" line="346"/>
        <source>This update requires re-importing all corporation transactions and journal.</source>
        <translation>Diese Update erfordert den Reimport aller Corporation Transaktionen und Logbücher.</translation>
    </message>
</context>
<context>
    <name>Evernus::WalletEntryFilterWidget</name>
    <message>
        <location filename="../WalletEntryFilterWidget.cpp" line="32"/>
        <source>Show:</source>
        <translation>Anzeigen:</translation>
    </message>
</context>
<context>
    <name>Evernus::WalletJournalModel</name>
    <message>
        <location filename="../WalletJournalModel.cpp" line="40"/>
        <source>Ignored</source>
        <translation>Ignoriert</translation>
    </message>
    <message>
        <location filename="../WalletJournalModel.cpp" line="41"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../WalletJournalModel.cpp" line="42"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../WalletJournalModel.cpp" line="43"/>
        <source>First party</source>
        <translation>Erster Partei</translation>
    </message>
    <message>
        <location filename="../WalletJournalModel.cpp" line="44"/>
        <source>Second party</source>
        <translation>Zweite Partei</translation>
    </message>
    <message>
        <location filename="../WalletJournalModel.cpp" line="45"/>
        <source>Additional data</source>
        <translation>Weitere Daten</translation>
    </message>
    <message>
        <location filename="../WalletJournalModel.cpp" line="46"/>
        <source>Amount</source>
        <translation>Betrag</translation>
    </message>
    <message>
        <location filename="../WalletJournalModel.cpp" line="47"/>
        <source>Balance after</source>
        <translation>Kontostand danach</translation>
    </message>
    <message>
        <location filename="../WalletJournalModel.cpp" line="48"/>
        <source>Reason</source>
        <translation>Grund</translation>
    </message>
</context>
<context>
    <name>Evernus::WalletJournalWidget</name>
    <message>
        <location filename="../WalletJournalWidget.cpp" line="55"/>
        <source>all</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../WalletJournalWidget.cpp" line="55"/>
        <source>incoming</source>
        <translation>Einnahmen</translation>
    </message>
    <message>
        <location filename="../WalletJournalWidget.cpp" line="55"/>
        <source>outgoing</source>
        <translation>Ausgaben</translation>
    </message>
    <message>
        <location filename="../WalletJournalWidget.cpp" line="61"/>
        <source>Combine for all characters</source>
        <translation>Für alle Charaktere zusammenfassen</translation>
    </message>
</context>
<context>
    <name>Evernus::WalletPreferencesWidget</name>
    <message>
        <location filename="../WalletPreferencesWidget.cpp" line="36"/>
        <source>Journal</source>
        <translation>Logbuch</translation>
    </message>
    <message>
        <location filename="../WalletPreferencesWidget.cpp" line="41"/>
        <location filename="../WalletPreferencesWidget.cpp" line="68"/>
        <source>Delete old entries</source>
        <translation>Alte Einträge löschen</translation>
    </message>
    <message>
        <location filename="../WalletPreferencesWidget.cpp" line="48"/>
        <location filename="../WalletPreferencesWidget.cpp" line="75"/>
        <source>Delete older than:</source>
        <translation>Lösche Einträge älter als:</translation>
    </message>
    <message>
        <location filename="../WalletPreferencesWidget.cpp" line="53"/>
        <location filename="../WalletPreferencesWidget.cpp" line="80"/>
        <source> days</source>
        <translation> Tage</translation>
    </message>
    <message>
        <location filename="../WalletPreferencesWidget.cpp" line="63"/>
        <source>Transactions</source>
        <translation>Transaktionen</translation>
    </message>
</context>
<context>
    <name>Evernus::WalletTransactionView</name>
    <message>
        <location filename="../WalletTransactionView.cpp" line="120"/>
        <source>Copy suggested price: %1</source>
        <translation>Vorgeschlagegen Preis kopieren: %1</translation>
    </message>
    <message>
        <location filename="../WalletTransactionView.cpp" line="162"/>
        <source>Add to item costs</source>
        <translation>Zu Kosten hinzufügen</translation>
    </message>
    <message>
        <location filename="../WalletTransactionView.cpp" line="170"/>
        <source>Add to item costs for:</source>
        <translation>Zu den Kosten für Gegenstand hinzufügen:</translation>
    </message>
    <message>
        <location filename="../WalletTransactionView.cpp" line="219"/>
        <source>Copy suggested price</source>
        <translation>Vorgeschlagegen Preis kopieren</translation>
    </message>
</context>
<context>
    <name>Evernus::WalletTransactionsModel</name>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="42"/>
        <source>Ignored</source>
        <translation>Ignoriert</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="43"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="44"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="45"/>
        <source>Quantity</source>
        <translation>Menge</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="46"/>
        <source>Item</source>
        <translation>Gegenstand</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="47"/>
        <source>Price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="48"/>
        <source>Character</source>
        <translation>Charakter</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="49"/>
        <source>Client</source>
        <translation>Kunde</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="50"/>
        <source>Location</source>
        <translation>Standort</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="104"/>
        <source>Buy</source>
        <translation>Kauf</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsModel.cpp" line="105"/>
        <source>Sell</source>
        <translation>Verkauf</translation>
    </message>
</context>
<context>
    <name>Evernus::WalletTransactionsWidget</name>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="63"/>
        <source>all</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="63"/>
        <source>buy</source>
        <translation>Kauf</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="63"/>
        <source>sell</source>
        <translation>Verkauf</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="69"/>
        <source>Combine for all characters</source>
        <translation>Für alle Charaktere zusammenfassen</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="102"/>
        <source>Total transactions:</source>
        <translation>Transaktionen gesamt:</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="108"/>
        <source>Total quantity:</source>
        <translation>Menge gesamt:</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="114"/>
        <source>Total size:</source>
        <translation>Größe gesamt:</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="120"/>
        <source>Total income:</source>
        <translation>Summe Einkünfte:</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="127"/>
        <source>Total cost:</source>
        <translation>Kosten gesamt:</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="134"/>
        <source>Total balance:</source>
        <translation>Bilanzsumme:</translation>
    </message>
    <message>
        <location filename="../WalletTransactionsWidget.cpp" line="140"/>
        <source>Total profit based on costs:</source>
        <translation>Profit gesamt (kostenbasiert):</translation>
    </message>
</context>
<context>
    <name>LMeveTask</name>
    <message>
        <location filename="../LMeveTask.cpp" line="32"/>
        <source>Missing JSON object!</source>
        <translation>Fehlendes JSON Objekt!</translation>
    </message>
    <message>
        <location filename="../LMeveTask.cpp" line="37"/>
        <source>Missing JSON value: %1</source>
        <translation>Fehlender JSON Wert: %1</translation>
    </message>
</context>
<context>
    <name>LocationBookmark</name>
    <message>
        <location filename="../LocationBookmark.cpp" line="54"/>
        <location filename="../LocationBookmark.cpp" line="55"/>
        <location filename="../LocationBookmark.cpp" line="56"/>
        <source>[all]</source>
        <translation>[Alle]</translation>
    </message>
</context>
<context>
    <name>MarketOrder</name>
    <message>
        <location filename="../MarketOrder.cpp" line="228"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="../MarketOrder.cpp" line="230"/>
        <source>Closed</source>
        <translation>Geschlossen</translation>
    </message>
    <message>
        <location filename="../MarketOrder.cpp" line="232"/>
        <source>Fulfilled</source>
        <translation>Ausgeführt</translation>
    </message>
    <message>
        <location filename="../MarketOrder.cpp" line="234"/>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <location filename="../MarketOrder.cpp" line="236"/>
        <source>Pending</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <location filename="../MarketOrder.cpp" line="238"/>
        <source>Deleted</source>
        <translation>Gelöscht</translation>
    </message>
    <message>
        <location filename="../MarketOrder.cpp" line="240"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>SecurityHelper</name>
    <message>
        <location filename="../SecurityHelper.cpp" line="48"/>
        <source>Security error</source>
        <translation>Sicherheitsfehler</translation>
    </message>
    <message>
        <location filename="../SecurityHelper.cpp" line="49"/>
        <source>Encountered SSL errors:
%1
Are you sure you wish to proceed (doing so can compromise your account security)?</source>
        <translation>Angetroffen SSL-Fehler:
%1
Sind Sie sicher, dass Sie den Vorgang fortsetzen (dabei können Ihre Kontosicherheit gefährden) möchten?</translation>
    </message>
</context>
<context>
    <name>TextUtils</name>
    <message>
        <location filename="../TextUtils.cpp" line="43"/>
        <source>%02dmin</source>
        <translation>%02d Min</translation>
    </message>
    <message>
        <location filename="../TextUtils.cpp" line="46"/>
        <source>%02dh %02dmin</source>
        <translation>%02d St %02d Min</translation>
    </message>
    <message>
        <location filename="../TextUtils.cpp" line="48"/>
        <source>%dd %02dh</source>
        <translation>%d Tg %02d St</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="59"/>
        <source>Evernus EVE Online trade tool</source>
        <translation>Evernus EVE Online Trade Tool</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>CREST client id.</source>
        <translation>CREST Client ID.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>CREST client secret.</source>
        <translation>CREST Client Secret.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="80"/>
        <location filename="../main.cpp" line="95"/>
        <source>Already running</source>
        <translation>Läuft bereits</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="80"/>
        <source>Evernus seems to be already running. If this is not the case, please remove &apos;%1&apos;.</source>
        <translation>Evernus scheint schon zu laufen. Is das nicht der Fall, entferne bitte &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="95"/>
        <source>Evernus probably didn&apos;t close cleanly the last time. Do you want to try to perform a cleanup?</source>
        <translation>Evernus wurde letztes Mal wahrscheinlich nicht korrekt beendet. Möchtest du einen Aufräumversuch starten?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="104"/>
        <location filename="../main.cpp" line="219"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="105"/>
        <source>Couldn&apos;t remove &apos;%1&apos;!</source>
        <translation>Konnte &apos;%1&apos; nicht entfernen!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="228"/>
        <source>Initialization error</source>
        <translation>Fehler beim Initialisieren</translation>
    </message>
</context>
</TS>
